<?php
require('fpdf.php');

class PDF extends FPDF{
	
	
	function Header(){
		$this->Image('topo_certificado.jpg',1,1,200); //logo
	    $this->SetFont('Arial','B',9);
		$this->Ln(18);//quebra de linha
		$this->setX(210);
		$this->Ln(5);//quebra de linha
	}
	
	// Page footer
	function Footer(){
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		// Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
	
}

// Instanciation of inherited class
//Instanciation of inherited class
$pdf=new PDF('L','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetLineWidth(0.2);
$pdf->Line(150,45,150,160);
$pdf->setY("40");
$pdf->setX("10");
$pdf->SetFont('Arial','',14);
$pdf->Cell(0,50,'Parab�ns,',0,1,'');
$pdf->setY("80");
$pdf->setX("10");
$pdf->Cell(0,0,'Voc� foi aprovado(a) e acaba de receber seu Certificado Web.',0,1,'');
$pdf->setY("90");
$pdf->Cell(0,0,'Favor imprimir as p�ginas 2 e 3  abaixo.',0,1,'');
$pdf->setY("100");
$pdf->setX("10");
$pdf->Cell(0,0,'Antes de apresent�-lo ao setor respons�vel verifique se ele',0,1,'');
$pdf->Cell(0,12,'cont�m  todas as caracter�sticas exigidas.',0,1,'');
$pdf->Cell(0,10,'Fa�a um teste no Autenticador de Certificados na p�gina',0,1,'');
$pdf->Cell(0,2,'http://www.imazon.com.br/autenticacao/ a fim de verificar',0,1,'');
$pdf->Cell(0,10,'se o documento est� sendo exibido normalmente.',0,1,'');

$pdf->Cell(0,10,'Em caso de d�vidas ou problemas entre em contato',0,1,'');
$pdf->Cell(0,2,'pelo e-mail atendimento@imazon.com.br.',0,1,'');

$pdf->Cell(0,30,'Atenciosamente,',0,1,'');
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,-20,'Equipe Imazon',0,1,'');


$pdf->setY("40");
$pdf->setX("160");
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,50,'Certificado Impresso Original',0,1,'');
$pdf->SetFont('Arial','',14);
$pdf->setY("80");
$pdf->setX("160");
$pdf->Cell(0,0,'Este certificado est� dispon�vel, tamb�m, na vers�o Original',0,1,'');
$pdf->setX("160");
$pdf->Cell(0,12,'para ser enviado pelos Correios.',0,1,'');

$pdf->setY("95");
$pdf->setX("160");
$pdf->Cell(0,0,'Para solicitar o envio basta navegar no Menu Principal at�',0,1,'');
$pdf->setX("160");
$pdf->Cell(0,12,'Certificado Impresso.',0,1,'');
$pdf->setX("160");
$pdf->Cell(0,12,'Gere e pague a fatura. Em at� 20 dias o documento',0,1,'');
$pdf->setX("160");
$pdf->Cell(0,0,'ser� entregue em seu endere�o.',0,1,'');


$pdf->setY("125");
$pdf->setX("160");
$pdf->Cell(0,12,'Click no bot�o PAGAR somente quando for solicitar o ',0,1,'');
$pdf->setX("160");

$pdf->Cell(0,0,'certificado, visto que iniciado o processo de pagamento',0,1,'');
$pdf->setX("160");

$pdf->Cell(0,12,'n�o poder� ser interrompido.',0,1,'');
$pdf->Output();

?>