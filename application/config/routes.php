<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['default_controller'] = 'Palestra/index';
$route['inicio'] = 'Palestra/index';
$route['inicio/(:any)'] = 'Palestra/index/$1';

$route['codifica_dados'] = 'Palestra/codifica_dados_enviados';

$route['busca_curso'] = 'Palestra/busca_curso';
$route['busca_curso/(:any)'] = 'Palestra/busca_curso/$1';

$route['lista_cursos'] = 'Palestra/busca_dinamica_curso';

$route['gera_certificado_palestra/(:any)/(:any)/(:any)/(:any)'] = 'Palestra/gera_certificado_palestra/$1/$2/$3/$4';
$route['gera_certificado_palestra_palestrante/(:any)/(:any)/(:any)/(:any)'] = 'Palestra/gera_certificado_palestra_palestrante/$1/$2/$3/$4';


$route['pagina_cadastro_palestra'] = 'Palestra/cadastro_palestra';
$route['pagamento_participante'] = 'Palestra/pagamento_participante';
$route['check_avaliacao'] = 'Palestra/check_avaliacao';
$route['avaliar'] = 'Palestra/avaliar_palestra';
//$route['palestra_matriculada/(:num)'] = 'palestra_matriculada';
$route['palestra_matriculada/(:any)'] = 'Palestra/palestra_matriculada/$1';
$route['palestra_matriculada'] = 'Palestra/palestra_matriculada';

$route['palestra_disponivel/(:any)'] = 'Palestra/palestra_disponivel/$1';
$route['palestra_disponivel'] = 'Palestra/palestra_disponivel';


$route['matricula'] = 'Historico/realiza_matricula';
$route['palestras_disponiveis'] = 'Palestra/palestras_disponiveis';
$route['retorno_pagamento'] = 'RetornoPagamento';
$route['pagamento_palestrante'] = 'Palestra/pagamento_palestrante';
$route['url_retorno'] = 'Palestra/url_retorno';
$route['send'] = 'Palestra/send';
$route['aceite'] = 'Palestra/do_aceite';
$route['termo'] = 'Palestra/termo';
$route['insert_termo'] = 'Palestra/insert_usuario_termo_uso';
$route['erro'] = 'Palestra/erro';
$route['consulta/(:any)'] = 'Palestra/consulta/$1';
$route['autentica'] = 'Palestra/autentica_instituicao';
$route['cliente'] = 'ClientePalestra/cliente';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
