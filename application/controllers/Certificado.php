<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Certificado extends CI_Controller
{
    
    public function get_certificado()
    {
        $valor = $this->input->post("valor", TRUE);
        $id_historico = decodifica($valor);
        
        $this->consulta_certificado($id_historico);
        
    }
    
    private function consulta_certificado($id_historico)
    {
        $this->load->model("Palestra_model", "pl");
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
        
        $palestra_usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
        $id_usuario = $palestra_usuario[0]['id_usuario'];
        
        if( !empty($id_usuario) && !empty($id_historico) )
        {
        
            $certificado = $this->pl->certificado($id_usuario, $id_historico);
            //var_dump($certificado);die;
            if($certificado['status']==TRUE)
            {
                $recuperar_arquivo_ovumdocs =  $this->recuperar_arquivo_ovumdocs($certificado['id_certificado']);
                echo json_encode($recuperar_arquivo_ovumdocs);
            }
            else
            {
                $add_arquivo_docs = $this->add_arquivo_docs($certificado['id_certificado']);
                echo json_encode($add_arquivo_docs);
            }
        }
        else
        {
            echo json_encode(['mensagem'=>'Faltam dados para consulta do certificado']);
        }
    }
    
    private function recuperar_arquivo_ovumdocs($identificador) {
      #Local da funcionalidade do Serviço OvumDocs. Nesse caso Função gerar Link
        $url =  DOCS_URL . 'link';

        #Chaves de acesso à API enviadas via POST, junto com URL de acesso ao arquivo, Id do Arquivo, e Descrição opcional
        $post = array(
            'chave'        => DOCS_CHAVE,
            'token'        => DOCS_TOKEN,
            'id_arquivo' => $identificador
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);


        #Captura Resposta de OvumDocs
        $requisicao = curl_exec($ch);#$requisição é um Objeto JSON
        $resposta = json_decode($requisicao, TRUE);#$requisição é um Objeto JSON
        curl_close($ch);

//        var_dump($resposta);
        #Exibição em tela do array de resposta
        return $resposta;
  }

    private function add_arquivo_docs($id_arquivo)
    {
        $url = DOCS_URL.'salvar';
        
        $post = array(
        'chave' => DOCS_CHAVE,
        'token' => DOCS_TOKEN,
        'url_arquivo' => base_url().'certificados/certificado.pdf',
        'id_arquivo' => $id_arquivo,
        'descricao' => ''
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        $requisicao = curl_exec($ch);
        $resposta = json_decode($requisicao, TRUE);
        curl_close($ch);
        
        return $resposta;
    }
}