<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RetornoPagamento extends CI_Controller
{
    public function index()
    {
        //{"transacao":"1181189460b771558340525","meiopagamento":"BOLETO","cod_status":1,"url_retorno":"https:\/\/www.meusite.com.br\/f\/","pedido":"meupedido5465454"}
        
        $resposta = $this->input->post("resposta", TRUE);
        $resposta = $this->security->xss_clean($resposta);
        
        $resposta_decodificada = json_decode($resposta);

        $cod_status = $resposta_decodificada->cod_status;
        $pedido = $resposta_decodificada->pedido;
        $codigo_intermediador = $resposta_decodificada->transacao;
        
        if($cod_status==3)
        {
            $this->load->model("Palestra_model","pl");
            
            $this->pl->update_situacao_pagamento($pedido, $codigo_intermediador, $cod_status);
        }
    }
}