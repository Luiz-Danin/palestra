<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Palestra extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
 
        // load Session Library
        $this->load->library('session');
         
        // load url helper
        $this->load->helper('url');
        
        $usuario = $this->session->userdata('usuario_logado');
        if(!$usuario)
        {
            //$data = ['mensagem'=>'usuario inválido'];
            //$this->erro($data);
        }
        
    }
    
    public function index()
    {
        //echo phpinfo();die;
        //echo 'incio';die;
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
        
        $this->load->model("Palestra_model","pl");
        $usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
        
        $minha_palestra = $this->show_minha_palestra($usuario_id_unico);
        //echo '<pre>';        var_dump($minha_palestra);die;
        $dados = [
            'minhas_palestas'=>$minha_palestra,
            'identificador_usuario'=>$usuario[0]['id_usuario']
        ];
        
        $this->load->view('site/paginas/index', $dados);
    }
    
    public function get_certificado()
    {
        $dados = $this->input->post();
        var_dump($dados);die;
        
        
    }
    
    private function consulta_certificado()
    {
        $this->pl->add_certificado( $id_usuario, $id_historico );
    }

    private function view_pagamento_palestrante($dados)
    {
        $this->load->view('aluno/paginas/pagamento', $dados);
    }
    
    private function view_pagamento_participante($dados)
    {
        $this->load->view('aluno/paginas/pagamento_participante', $dados);
    }
    
    public function cadastro_palestra()
    {
        $this->load->model("Palestra_model","pl");
        
        $this->load->view('aluno/paginas/pagina_cadastro_palestra');
    }

    public function pagamento_palestrante()
    {
        $this->load->model("Palestra_model","pl");
        
        $id_palestra_codificada = $this->input->post("id_palestra", TRUE);
        $id_palestra = decodifica($id_palestra_codificada);
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
        
        $usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
        $id_usuario = $usuario[0]['id_usuario'];
        
        $usuario_palestrante = $this->pl->get_usuario_palestrante($id_usuario);
        
        if ( !empty($id_palestra) && ( count($usuario_palestrante)>0 ) )
        {
            $id_historico = $this->pl->add_historico($usuario_palestrante['id_usuario'], $id_palestra);
            
            if( !empty($id_historico) )
            {
                $id_transacao = $this->gera_id_transacao($id_usuario);
                $descricao = 'Compra Certificado de Palestrante';
                $resposta = $this->gera_transacao_ovum($usuario[0], $id_transacao, $descricao, 20);
                $resposta_decodificada = json_decode($resposta, true);
                
                if( isset( $resposta_decodificada['situacao'] ) && ($resposta_decodificada['situacao']=='Sucesso') )
                {
                    $validade = date("Y-m-d");
                    //$cupom = $this->gerar_cupom($validade);
                    
                    $codigo_intermediador = $resposta_decodificada['dados'][0]['transacao'];
                    $this->pl->add_usuario_palestrante_pagamento($id_usuario, $id_historico, $id_transacao, $codigo_intermediador);
                    
                    $dados = [ 'identificador'=>$resposta_decodificada['dados'][0]['identificador'] ];
                    
                    $this->view_pagamento_palestrante($dados);
                }
            }
        }
        else
        {
            $dado = ['mensagem'=>'Falta parametros para o processamento do pagamento'];
            $this->erro($dado);
        }
    }
    
    private function gera_id_transacao($id_usuario)
    {
        $posfix = strtoupper(substr(md5(uniqid() . $id_usuario ), 0, 12));

        return "PAL" . $posfix;
    }
    
    private function gera_transacao_ovum($aluno, $transacao, $descricao, $valor)
    {
        if( empty($aluno['nome']) )
        {
            $aluno['nome'] = 'Não informado';
        }
        
        $url = PAGAMENTO_URL;
        $data["autenticacao"]["chave"] = PAGAMENTO_CHAVE;
        $data["autenticacao"]["token"] = PAGAMENTO_TOKEN;
        $data["cliente"]["id"] = $aluno['id_usuario'];
        $data["cliente"]["nome"] = $aluno['nome'];
        $data["cliente"]["email"] = $aluno['email'];
        $data["transacao"]["tipo"] = "S";
        $data["transacao"]["descricao"] = $descricao;
        $data["transacao"]["valor"] = $valor;
        $data["transacao"]["pedido"] = $transacao;
        $data["loja"]["url_retorno"] = base_url()."retorno_pagamento";
        
        $data = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return curl_exec($ch);
    }
    
    public function consulta_avaliacao_palestra($historico)
    {
        //echo '<pre>';var_dump($historico);die;
        $consulta_avaliacao_palestra = $this->pl->consulta_avaliacao_palestra($historico['id_usuario'], $historico['id_historico'], $historico['id_palestra']);

        if( count($consulta_avaliacao_palestra) >0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    private function palestra_historico_usuario($id_usuario,  $limit=null, $start=null)
    {
        if( !empty($id_usuario) )
        {
            //$historico_usuario =  $this->pl->get_historico_usuario($id_usuario);
            
            $historico_usuario = $this->pl->get_historico_usuario_2($id_usuario, $limit, $start);
            //echo '<pre>';var_dump($historico_usuario_2);die;
            $palestra_historico_usuario = [];
            
            for ($i = 0 ; $i<count($historico_usuario) ; $i++)
            {
                $result = $this->pl->get_palestra_by_id_palestra($historico_usuario[$i]['id_palestra']);
                
                $result[0]['pago'] = $this->pl->check_pagamento_historico($historico_usuario[$i]['id_usuario'], $historico_usuario[$i]['id_historico']);
                
                $result[0]['avaliado'] = $this->consulta_avaliacao_palestra($historico_usuario[$i]);
                
                if($result[0]['pago'])
                {
                    
                    //$add_arquivo_docs = $this->add_arquivo_docs( $historico_usuario[$i]['id_historico'] );
                    $cod_autenticacao = $this->gera_cod_autenticacao($historico_usuario[$i]['id_usuario']);
                    
                    $id = $historico_usuario[$i]['id_usuario'] + $historico_usuario[$i]['id_historico'];
                    $identificador = $this->gera_identificador($id);
                    
                    $certificado = $this->pl->certificado_participante($historico_usuario[$i]['id_usuario'], $historico_usuario[$i]['id_historico'], $cod_autenticacao, $identificador);
                    //echo '<pre>';var_dump($certificado);die;
                    if( $certificado['status'] )
                    {
                        $recuperar_arquivo_ovumdocs =  $this->recuperar_arquivo_ovumdocs( $certificado['codigo_autenticacao'] );
                        
                        if( isset($recuperar_arquivo_ovumdocs['url']) )
                        {
                            $result[0]['url_certificado'] = $recuperar_arquivo_ovumdocs['url'];
                        }
                        else
                        {
                            $result[0]['url_certificado'] = "";
                        }
                    }
                    else
                    {
                        $id_certificado = $certificado['codigo_autenticacao']['id_certificado'];
                        $id_historico = $historico_usuario[$i]['id_historico'];
                        
                        //$this->gera_certificado_palestra($id_certificado, $historico_usuario[$i]['id_usuario'], $historico_usuario[$i]['id_palestra'], $id_historico);
                        
                        $url_certificado = base_url().'gera_certificado_palestra/'.codifica($id_certificado).'/'.codifica($historico_usuario[$i]['id_usuario']).'/'. codifica($historico_usuario[$i]['id_palestra']).'/'.codifica($id_historico);
                        //var_dump($url_certificado);die;
                        $add_arquivo_docs = $this->add_arquivo_docs( $certificado['codigo_autenticacao']['codigo_autenticacao'], $url_certificado );
                        
                        if( isset($add_arquivo_docs['url']) )
                        {
                            $result[0]['url_certificado'] = $add_arquivo_docs['url'];
                        }
                        else
                        {
                            $result[0]['url_certificado'] = "";
                        }
                    }
                    
                    /*if( isset($add_arquivo_docs['url']) )
                    {
                        $result[0]['url_certificado'] = $add_arquivo_docs['url'];
                    }
                    else
                    {
                        $recuperar_arquivo_ovumdocs =  $this->recuperar_arquivo_ovumdocs( $historico_usuario[$i]['id_historico'] );
                        $result[0]['url_certificado'] = $recuperar_arquivo_ovumdocs['url'];
                    }*/
                    
                }
                
                
                
                $result[0]['matriculado'] = TRUE;
                $result[0]['id_historico'] = $historico_usuario[$i]['id_historico'];
                
                $palestra_historico_usuario = array_merge($palestra_historico_usuario, $result);
            }
            //echo '<pre>';var_dump($palestra_historico_usuario);die;
            
            return $palestra_historico_usuario;
        }
    }
    
    public function gera_certificado_palestra($id_certificado, $id_usuario, $id_palestra, $id_historico)
    {
        
        $id_certificado = decodifica($id_certificado);
        $id_usuario = decodifica($id_usuario);
        $id_palestra = decodifica($id_palestra);
        $id_historico = decodifica($id_historico);
        
        $this->load->model("Palestra_model","pl");
        
        $certificado_palestra = $this->pl->get_certificado_palestra($id_certificado, $id_usuario, $id_palestra, $id_historico);
        //echo '<pre>';        var_dump($certificado_palestra);die;
        //echo '<pre>';var_dump($id_certificado.'/'.$id_usuario.'/'.$id_palestra.'/'.$id_historico);die;
        $id_usuario_palestrante = $certificado_palestra[0]['id_usuario_palestrante'];
        $usuario = $this->pl->get_usuario($id_usuario_palestrante);
        $nome_palestrante = $usuario['nome'];
        
        $is_pago = $this->pl->is_pago($id_usuario, $id_historico);
        //var_dump($is_pago);die;
        
        if($is_pago)
        {
            
            if( count($certificado_palestra)>0 )
            {

                require_once(APPPATH.'libraries/Fpdf/Fpdf.php');
                $this->fpdf = new FPDF('l');

                $this->fpdf->AliasNbPages();                
                $this->fpdf->AddPage();
                $this->fpdf->Image(base_url().'public/img/certificado_palestra_ref.png', -2, 0, 300, 'JPG'); //logo
                $this->fpdf->setY("38");
                $this->fpdf->setX("5");
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 10, 'conferido a', 0, 1, 'C');
                $this->fpdf->SetFont('Arial', 'B', 16);
                $this->fpdf->Cell(0, 6, utf8_decode($certificado_palestra[0]['nome']), 0, 1, 'C');

                    if( $certificado_palestra[0]['documento'] != ""){
                        $this->fpdf->SetFont('Arial', '', 16);
                        $this->fpdf->Cell(0, 7, 'Documento: ' . utf8_decode($certificado_palestra[0]['documento']), 0, 1, 'C');
                        $this->fpdf->SetFont('Arial', '', 16);
                        $this->fpdf->Cell(0, 7, utf8_decode("Por ter participado da Palestra"), 0, 1, 'C');
                    }else{
                        $this->fpdf->setY("61");
                        $this->fpdf->SetFont('Arial', '', 16);
                        $this->fpdf->Cell(0, 7, utf8_decode("Por ter participado da Palestra"), 0, 1, 'C');
                    }

                    $descricao_palestra = wordwrap( $certificado_palestra[0]['descricao_palestra'], 60, "\n", true);
                    //$descricao_palestra = utf8_decode($descricao_palestra);

                    //$this->fpdf->setY(98);
                    //$this->fpdf->MultiCell(0, 6, utf8_decode("Descrição da Palestra: ".$descricao_palestra), 0, 'C');
                    $this->fpdf->setY(99);
                    $this->fpdf->setX(52);
                    $this->fpdf->MultiCell(210, 6, utf8_decode("Descrição da Palestra: \n".$descricao_palestra), 50, 'C');

                    //$this->fpdf->setY(122);
                    //$this->fpdf->Cell(0, 10, utf8_decode("Autenticar em:  https://www.aporfia.com.br/imazoncursos/autenticacao"), 0, 1, 'C');

                    //$this->fpdf->setX(90);
                    //$this->fpdf->setY(98);
                    //$this->fpdf->Cell(0, 10, utf8_decode("Código de Autenticação: ".$certificado_palestra[0]['codigo_autenticacao'] ), 0, 1);

                    $nome_palestra = wordwrap( $certificado_palestra[0]['nome_palestra'], 60, "\n", true);
                    $nome_palestra = utf8_decode($nome_palestra);
                    //$nome_curso_original = utf8_decode($historico[0]->nomeCurso);


                    /*if($certificado_palestra[0]['cpf'] != ""){
                        $this->fpdf->setY(88);
                        $this->fpdf->MultiCell(0, 6, $nome_curso, 0, 'C');
                    }else{
                        $this->fpdf->setY(88);
                        $this->fpdf->MultiCell(0, 6, $nome_curso, 0, 'C');
                    }*/

                    $this->fpdf->setY(68);
                    $this->fpdf->MultiCell(0, 6, $nome_palestra, 0, 'C');

                    $this->fpdf->setY(74);
                    $this->fpdf->MultiCell(0, 6, "Ministrada por ".$nome_palestrante, 0, 'C');

                    //Inicio de ajuste para nome de curso que excede o limite da linha
                    $this->fpdf->setY(85);
                    $this->fpdf->setX(50);
                    //$this->fpdf->Cell(0, 5, utf8_decode('Tipo: Palestra'), 0, 1);
                    $this->fpdf->setX(50);
                    //$this->fpdf->Cell(0, 5, 'Aproveitamento', 0, 1);
                    $this->fpdf->setX(50);
                    $this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº: ') . $certificado_palestra[0]['identificador'], 0, 1);

                    $this->fpdf->setY(93);
                    $this->fpdf->setX(50);
                    //$this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº: ') . $certificado_palestra[0]['identificador'], 0, 1);
                    $this->fpdf->Cell(0, 1, utf8_decode('Código de autenticação: ').$certificado_palestra[0]['codigo_autenticacao'], 0, 1);

                    $this->fpdf->setY(85);

                    $this->fpdf->setX(180);
                    //$this->fpdf->Cell(0, 5, utf8_decode('Carga Horária: ') . 'carga_horaria' . ' Horas', 0, 1);
                    $this->fpdf->setX(180);
    //                var_dump(utf8_decode($array_dados_certficado['data_inicio']));
    //                die;date('d/m/Y', strtotime($certificado_palestra[0]['data_avaliacao']));
                    $this->fpdf->Cell(0, 5, utf8_decode("Data: ".date('d/m/Y', strtotime( $certificado_palestra[0]['data_avaliacao'] ) ) ), 0, 1);

                    $this->fpdf->setX(180);
                    $this->fpdf->Cell(0, 5, 'Autenticar em: www.ovumdoc.com', 0, 1);

                    $this->fpdf->setX(180);
                    //$this->fpdf->Cell(0, 5, utf8_decode($array_dados_certficado['data_termino']), 0, 1);
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->SetXY(10, 125);
                    //$texto = utf8_decode("Lei N° 9394/96, Art 40, Resolução CNE/CEB Nº 6/2012, Art. 25º. Decreto 5154/2004, Art. 3°");
                    //$this->fpdf->MultiCell(0, 5, $texto, 0, 'C');
                    //$this->fpdf->SetFont('Arial', '', 16);
                    //$this->fpdf->Cell(430, 13, utf8_decode('Belém, ') . date("d/m/Y"), 0, 1, 'C');
                    //$this->fpdf->SetFont('Arial', '', 16);

                    //$this->fpdf->Line(20, 45, 20, 45);
                    //$this->fpdf->Line(180, 175, 270, 175);
                    //inicio
                    //$this->fpdf->SetFont('Arial', '', 16);

                $this->fpdf->SetLineWidth(0.2);

                $this->fpdf->Line(40, 170, 125, 170);

                $this->fpdf->SetXY(50, 175);

                $this->fpdf->Cell(0, 1, 'Assinatura do(a) Aluno(a)', 0, 0);


                $this->fpdf->Image('public/img/rubrica.jpg', 210, 150, 40);

                //$this->fpdf->Line(180, 160, 265, 160);

                //$this->fpdf->SetXY(190, 165);

                //$this->fpdf->Cell(0, 1, 'Prof. Me. Ezelildo G Dornelas', 0, 0);
                $this->fpdf->Line(199, 170, 265, 170);

                $this->fpdf->SetXY(198, 175);

                $this->fpdf->Cell(0, 1, 'Prof. Me. Ezelildo G Dornelas', 0, 0);

                $this->fpdf->SetXY(228, 180);

                $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);

                /*$this->fpdf->SetXY(140, 181);
                $this->fpdf->SetFont('Arial', '', 14);
                $this->fpdf->Cell(0, 1, 'Autenticar em: '.base_url().'autenticacao/', 0, 0);

                $this->fpdf->SetXY(140, 187);
                $this->fpdf->SetFont('Arial', '', 14);
                $this->fpdf->Cell(0, 1, utf8_decode('Código de autenticação: ').$certificado_palestra[0]['codigo_autenticacao'], 0, 0);*/
                    // Página de Verso	
                    //$pdf->Output("","S");
                return $this->fpdf->Output();
            }
        }
    }
    
    public function gera_certificado_palestra_palestrante($id_certificado, $id_usuario, $id_palestra, $id_historico)
    {
        
        $id_certificado = decodifica($id_certificado);
        $id_usuario = decodifica($id_usuario);
        $id_palestra = decodifica($id_palestra);
        $id_historico = decodifica($id_historico);
        
        $this->load->model("Palestra_model","pl");
        
        $usuario_palestrante = $this->pl->get_usuario_palestrante($id_usuario);
        
        $is_pago = $this->pl->is_pago($usuario_palestrante['id_usuario'], $id_historico);
        
        if($is_pago)
        {
            
        //echo '<pre>';var_dump($is_pago);die;
        
            $certificado_palestra = $this->pl->get_certificado_palestra_palestrante($id_certificado, $id_usuario, $id_palestra, $id_historico);
            //echo '<pre>';var_dump($certificado_palestra);die;

            $id_usuario_palestrante = $certificado_palestra[0]['id_usuario_palestrante'];
            $usuario = $this->pl->get_usuario($id_usuario_palestrante);
            $nome_palestrante = $usuario['nome'];

            if( count($certificado_palestra)>0 )
            {

                require_once(APPPATH.'libraries/Fpdf/Fpdf.php');
                $this->fpdf = new FPDF('l');

                $this->fpdf->AliasNbPages();                
                $this->fpdf->AddPage();
                $this->fpdf->Image(base_url().'public/img/certificado_palestra_ref.png', -2, 0, 300, 'JPG'); //logo
                $this->fpdf->setY("38");
                $this->fpdf->setX("5");
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 10, 'conferido a', 0, 1, 'C');
                $this->fpdf->SetFont('Arial', 'B', 16);
                $this->fpdf->Cell(0, 6, utf8_decode($certificado_palestra[0]['nome']), 0, 1, 'C');

                    if( $certificado_palestra[0]['documento'] != ""){
                        $this->fpdf->SetFont('Arial', '', 16);
                        $this->fpdf->Cell(0, 7, 'Documento: ' . utf8_decode($certificado_palestra[0]['documento']), 0, 1, 'C');
                        $this->fpdf->SetFont('Arial', '', 16);
                        $this->fpdf->Cell(0, 7, utf8_decode("Por ter Ministrado a Palestra"), 0, 1, 'C');
                    }else{
                        //$this->fpdf->SetFont('Arial', '', 16);
                        //$this->fpdf->Cell(0, 7, utf8_decode("Por ter Ministrado a Palestra"), 0, 1, 'C');

                        $this->fpdf->setY("61");
                        $this->fpdf->SetFont('Arial', '', 16);
                        $this->fpdf->Cell(0, 7, utf8_decode("Por ter Ministrado a Palestra"), 0, 1, 'C');

                    }

                    $descricao_palestra = wordwrap( $certificado_palestra[0]['descricao_palestra'], 60, "\n", true);
                    //$descricao_palestra = utf8_decode($descricao_palestra);

                    $this->fpdf->setY(99);
                    $this->fpdf->setX(52);
                    $this->fpdf->MultiCell(210, 6, utf8_decode("Descrição da Palestra: \n".$descricao_palestra. ' Cargas de escritas síncronas aleatórias extremas de I/O de rede e discos, para arquivos pequenos e curtas requisições de rede em ambientes corporativos estão presentes em servidores de e-mail e Cargas de escritas síncronas aleatórias extremas de I/O '), 50, 'C');

                    //$this->fpdf->setY(122);
                    //$this->fpdf->Cell(0, 10, utf8_decode("Autenticar em:  https://www.aporfia.com.br/imazoncursos/autenticacao"), 0, 1, 'C');

                    //$this->fpdf->setX(90);
                    //$this->fpdf->setY(98);
                    //$this->fpdf->Cell(0, 10, utf8_decode("Código de Autenticação: ".$certificado_palestra[0]['codigo_autenticacao'] ), 0, 1);

                    $nome_palestra = wordwrap( $certificado_palestra[0]['nome_palestra'], 60, "\n", true);
                    $nome_palestra = utf8_decode($nome_palestra);
                    //$nome_curso_original = utf8_decode($historico[0]->nomeCurso);


                    /*if($certificado_palestra[0]['cpf'] != ""){
                        $this->fpdf->setY(88);
                        $this->fpdf->MultiCell(0, 6, $nome_curso, 0, 'C');
                    }else{
                        $this->fpdf->setY(88);
                        $this->fpdf->MultiCell(0, 6, $nome_curso, 0, 'C');
                    }*/

                    $this->fpdf->setY(67);
                    $this->fpdf->MultiCell(0, 6, $nome_palestra, 0, 'C');

                    //$this->fpdf->setY(68);
                    //$this->fpdf->MultiCell(0, 6, "Ministrada por ".$nome_palestrante, 0, 'C');

                    //Inicio de ajuste para nome de curso que excede o limite da linha
                    $this->fpdf->setY(85);
                    $this->fpdf->setX(50);
                    //$this->fpdf->Cell(0, 5, utf8_decode('Tipo: Palestra'), 0, 1);
                    $this->fpdf->setX(50);
                    //$this->fpdf->Cell(0, 5, 'Aproveitamento', 0, 1);
                    $this->fpdf->setX(50);
                    $this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº: ') . $certificado_palestra[0]['identificador'], 0, 1);

                    $this->fpdf->setY(93);
                    $this->fpdf->setX(50);
                    //$this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº: ') . $certificado_palestra[0]['identificador'], 0, 1);
                    $this->fpdf->Cell(0, 1, utf8_decode('Código de autenticação: ').$certificado_palestra[0]['codigo_autenticacao'], 0, 1);


                    $this->fpdf->setY(85);

                    $this->fpdf->setX(180);
                    //$this->fpdf->Cell(0, 5, utf8_decode('Carga Horária: ') . 'carga_horaria' . ' Horas', 0, 1);
                    $this->fpdf->setX(180);
    //                var_dump(utf8_decode($array_dados_certficado['data_inicio']));
    //                die;date('d/m/Y', strtotime($certificado_palestra[0]['data_avaliacao']));
                    $this->fpdf->Cell(0, 5, utf8_decode("Data: ".date('d/m/Y', strtotime( $certificado_palestra[0]['data_palestra'] ) ) ), 0, 1);

                    //$this->fpdf->setY(95);
                    $this->fpdf->setX(180);
                    $this->fpdf->Cell(0, 5, 'Autenticar em: www.ovumdoc.com', 0, 1);

                    $this->fpdf->setX(180);
                    //$this->fpdf->Cell(0, 5, utf8_decode($array_dados_certficado['data_termino']), 0, 1);
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->SetXY(10, 125);
                    //$texto = utf8_decode("Lei N° 9394/96, Art 40, Resolução CNE/CEB Nº 6/2012, Art. 25º. Decreto 5154/2004, Art. 3°");
                    //$this->fpdf->MultiCell(0, 5, $texto, 0, 'C');
                    //$this->fpdf->SetFont('Arial', '', 16);
                    //$this->fpdf->Cell(430, 13, utf8_decode('Belém, ') . date("d/m/Y"), 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', '', 16);

                    //$this->fpdf->Line(20, 45, 20, 45);
                    //$this->fpdf->Line(180, 175, 270, 175);
                    //inicio
                    //$this->fpdf->SetFont('Arial', '', 16);

                $this->fpdf->SetLineWidth(0.2);

                $this->fpdf->Line(40, 170, 125, 170);

                $this->fpdf->SetXY(45, 175);

                $this->fpdf->Cell(0, 1, 'Assinatura do(a) Ministrante', 0, 0);


                $this->fpdf->Image('public/img/rubrica.jpg', 210, 150, 40);

                $this->fpdf->Line(199, 170, 265, 170);

                $this->fpdf->SetXY(198, 175);

                $this->fpdf->Cell(0, 1, 'Prof. Me. Ezelildo G Dornelas', 0, 0);

                $this->fpdf->SetXY(228, 180);

                $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);

                /*$this->fpdf->SetXY(140, 181);
                $this->fpdf->SetFont('Arial', '', 14);
                $this->fpdf->Cell(0, 1, 'Autenticar em: www.ovumdoc.com', 0, 0);*/

                /*$this->fpdf->SetXY(140, 187);
                $this->fpdf->SetFont('Arial', '', 14);
                $this->fpdf->Cell(0, 1, utf8_decode('Código de autenticação: ').$certificado_palestra[0]['codigo_autenticacao'], 0, 0);*/
                    // Página de Verso	
                    //$pdf->Output("","S");
                return $this->fpdf->Output();
            }
        }
    }

    private function gerar_cupom($validade)
    {
        //Parâmetros a serem enviados.
        $param = [
        'chave'         => CUPOM_CHAVE,
        'token'         => CUPOM_TOKEN,
        'instituicao'   => CUPOM_INSTITUICAO,
        'qtde_cupons'   => '1',
        'promocao'      => '13',
        'dt_validade'   => $validade,
        'valor'         => '10.00',
        'tipo_cupom'    => '1',
        'cupom'         => "PL".substr(md5(microtime()), 0, 6)
        ];
        //Conversão de dados em formato JSON.
        $dados = json_encode($param);
        $ch = curl_init(CUPOM_URL_GERAR);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result_decode = json_decode($result);
        
        $validade_formatada = date("d/m/Y", strtotime($validade));
        
        if($result_decode->situacao == "Sucesso")
        {
            return array($result_decode->cupom[0], $validade_formatada);
        }
        else
        {
            return NULL;
        }
        
    } 

    public function pagamento_participante()
    {
        
        $id_palestra_cod = $this->input->post("id_palestra", TRUE);
        $id_palestra = decodifica($id_palestra_cod);
        
        $id_historico_cod = $this->input->post("id_historico", TRUE);
        $id_historico = decodifica($id_historico_cod);
        
        $cupom = $this->input->post("cupom", TRUE);
        
        
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
        
        $this->load->model("Palestra_model","pl");
        $usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
        $id_usuario = $usuario[0]['id_usuario'];
        
        if ( !empty($id_palestra) && !empty($id_usuario) && !empty($id_historico) )
        {
            
            $id_transacao = $this->gera_id_transacao($id_usuario);
            $descricao = 'Compra Certificado de Participante';
            $resposta = $this->gera_transacao_ovum($usuario[0], $id_transacao, $descricao, 10);
            $resposta_decodificada = json_decode($resposta, true);
            //var_dump($resposta_decodificada);die;
            if( isset( $resposta_decodificada['situacao'] ) && ($resposta_decodificada['situacao']=='Sucesso') )
            {
                $codigo_intermediador = $resposta_decodificada['dados'][0]['transacao'];
                $this->pl->add_usuario_participante_pagamento($id_usuario, $id_historico, $id_transacao, $codigo_intermediador);
                
                $validade = date("Y-m-d");
                
                $dados = [
                            'cupom'=>$cupom,
                            'identificador'=>$resposta_decodificada['dados'][0]['identificador']
                         ];
                
                $this->view_pagamento_participante($dados);
            }
            else
            {
                $dado =  ['mensagem'=>'Falha do intermedador ao processar a transacao'];
                $this->erro($dado);
            }
        }
        else
        {
            $dado =  ['mensagem'=>'Falta parametros para o processamento do pagamento '];
            $this->erro($dado);
        }
        
        
    }

    public function check_avaliacao()
    {
        $this->load->model("Palestra_model","pl");
        
        $id_historico_cod = $this->input->post("id_h", TRUE);
        $id_historico = decodifica($id_historico_cod);
        
        $id_palestra_cod = $this->input->post("id", TRUE);
        $id_palestra = decodifica($id_palestra_cod);
        
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
            
        $usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
        
        $consulta_avaliacao_palestra = $this->pl->consulta_avaliacao_palestra($usuario[0]['id_usuario'], $id_historico, $id_palestra);
        
        
        //var_dump($id_palestra);
        //var_dump($id_historico);
        //die;
        
        if( count($consulta_avaliacao_palestra)>0)
        {
            echo json_encode( ['status'=>1,'dados'=>$consulta_avaliacao_palestra[0]],  JSON_FORCE_OBJECT );
        }
        else
        {
            echo json_encode( ['status'=>0, 'dados'=>NULL],  JSON_FORCE_OBJECT );
        }
        
    }

    public function avaliar_palestra()
    {
        
        $valor = $this->input->post("valor", TRUE);
        
        $id_historico_cod = $this->input->post("id_h", TRUE);
        $id_historico = decodifica($id_historico_cod);
        
        $id_palestra_cod = $this->input->post("id", TRUE);
        $id_palestra = decodifica($id_palestra_cod);
        
        //var_dump($id_historico);die;
        if(!empty($valor) && !empty($id_historico) && !empty($id_palestra) )
        {
            $this->load->model("Palestra_model","pl");
            
            $usuario_id_unico = $this->session->userdata('usuario_id_unico');
            
            $usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
            
            $avaliacao_palestra = $this->pl->avaliacao_palestra($usuario[0]['id_usuario'], $valor, $id_historico, $id_palestra);
            
            if($avaliacao_palestra['status']==FALSE)
            {
                $validade = date("Y-m-d");
                $cupom = $this->gerar_cupom($validade);
                $this->pl->add_palestra_avaliacao_cupom($avaliacao_palestra['id_avaliacao'], $cupom[0]);
            }
        }
        
    }
    
    public function busca_curso()
    {
        $busca_post = $this->input->post("busca", TRUE);
        
        $busca = filter_var($busca_post, FILTER_SANITIZE_STRING);
        
        if( !empty( trim($busca) ) )
        {
            $this->load->model("Palestra_model","pl");
            
            $resultado = $this->pl->get_busca_palestra($busca);
            //$this->pl->get_busca_palestra_2($busca, $limit, $start );
            
            $this->load->library("pagination");
        
            $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

            $config = array();
            //
            $config['full_tag_open'] = '<ul style="margin-left:15px; margin-right: 15px" class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['attributes'] = ['class' => 'page-link'];
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['prev_link'] = 'Anterior&laquo';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Proxima&raquo';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item active"><a href="'.base_url().'busca_curso/'.$page.'" class="page-link">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            //

            $config["base_url"] = base_url() . "busca_curso";
            $config["total_rows"] = count( $resultado );
            $config["per_page"] = PER_PAGE;
            $config["uri_segment"] = 2;

            $this->pagination->initialize($config);
            $links = $this->pagination->create_links();
            //


            $result = $this->pl->get_busca_palestra_2($busca, $config["per_page"], $page);

            $data = ['palestras_busca'=>$result, 'links'=>$links];    

            $this->load->view('aluno/paginas/resultado_busca_palestra', $data);
        }
        else
        {
            $data = ['palestras_busca'=>[]];
            $this->load->view('aluno/paginas/resultado_busca_palestra', $data);
        }
    }

    public function busca_dinamica_curso()
    {
        $busca_post = $this->input->post("search", TRUE);
        
        $busca = filter_var($busca_post, FILTER_SANITIZE_STRING);
        
        if( !empty( trim($busca) ) )
        {
            $this->load->model("Palestra_model","pl");
            
            $resultado = $this->pl->get_busca_palestra($busca);
            
            if( count($resultado)>0 )
            {
                foreach ($resultado as $src)
                {
                    //foreach ($src as $sr)
                    //{
                        //var_dump($src['nome']);die;
                        $pesquisa[] = $src['nome'];
                    //}
                }

                echo json_encode($pesquisa);
            }
        }
        
    }

    public function palestra_matriculada()
    {
        //die("palestra_matriculada");
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
         
        $this->load->model("Palestra_model","pl");
        
        $palestra_usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
        //echo '<pre>';var_dump($palestra_usuario[0]['id_usuario']);die;
        
        $this->load->library("pagination");
        
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        
        $config = array();
        //
        $config['full_tag_open'] = '<ul style="margin-left:15px; margin-right: 15px" class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = ['class' => 'page-link'];
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Proxima&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="'.base_url().'palestra_matriculada/'.$page.'" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        //
        
        $config["base_url"] = base_url() . "palestra_matriculada";
        $config["total_rows"] = count( $this->pl->get_historico_usuario($palestra_usuario[0]['id_usuario']) );
        $config["per_page"] = PER_PAGE;
        $config["uri_segment"] = 2;
        
        //var_dump($page);die;
        

        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();

        $palestra_historico_usuario = $this->palestra_historico_usuario($palestra_usuario[0]['id_usuario'], $config["per_page"], $page);
        
        if(count($palestra_historico_usuario)>0 )
        {
            //$get_palestras_disponiveis = $this->pl->get_palestras_disponiveis();
            
            //$count_palestras_disponiveis = count($palestra_historico_usuario);
            
            //$result = array_merge($palestra_historico_usuario, $get_palestras_disponiveis);
            $result = $palestra_historico_usuario;
            
            $result = $this->get_link_palestra($result);
            
            $result = $this->get_cupom_avaliacao($result);
            //echo '<pre>';var_dump($result);die;
            $data = ['palestras_disponiveis'=>$result,'links'=>$links, 'identificador_usuario'=>$palestra_usuario[0]['id_usuario']];
            
            $this->load->view('aluno/paginas/palestra_matriculada', $data);
        }
        else
        {
            $data = ['palestras_disponiveis'=>[]];
            $this->load->view('aluno/paginas/palestra_matriculada', $data);
        }
    }

    public function palestra_disponivel()
    {
        $this->load->model("Palestra_model","pl");
        
        
        //
        
        $this->load->library("pagination");
        
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        
        $config = array();
        //
        $config['full_tag_open'] = '<ul style="margin-left:15px; margin-right: 15px" class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = ['class' => 'page-link'];
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Proxima&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="'.base_url().'palestra_disponivel/'.$page.'" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        //
        
        $config["base_url"] = base_url() . "palestra_disponivel";
        $config["total_rows"] = count( $this->pl->get_palestras_disponiveis() );
        $config["per_page"] = PER_PAGE;
        $config["uri_segment"] = 2;
        
        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();
        //
        
        
        $result = $this->pl->get_palestras_disponiveis_2($config["per_page"], $page);

        $data = ['palestras_disponiveis'=>$result, 'links'=>$links];    

        $this->load->view('aluno/paginas/palestra_disponivel', $data);
    }
    
    private function get_cupom_avaliacao($palestras)
    {
        
        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
        
        $usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
        $id_usuario = $usuario[0]['id_usuario'];
        
        $avaliacao_palestra = [];
        $i = 0;
        
        foreach ($palestras as $palestra)
        {
            if( isset($palestra['id_historico']) )
            {
                $avaliacao_palestra = $this->pl->consulta_avaliacao_palestra($id_usuario, $palestra['id_historico'], $palestra['id_palestra']);
                
                if( isset($avaliacao_palestra[0]['id_avaliacao']) )
                {
                    $palestra_avaliacao_cupom = $this->pl->consulta_palestra_avaliacao_cupom($avaliacao_palestra[0]['id_avaliacao']);
                
                
                    if( isset($palestra_avaliacao_cupom[0]['cupom']))
                    {
                        $palestras[$i]['cupom'] = $palestra_avaliacao_cupom[0]['cupom'];
                    }
                }
            }
            
            $i++;
        }
        
        return $palestras;
        //echo '<pre>';var_dump($palestras);die;
    }

    private function get_link_palestra($palestras)
    {
        $i = 0;
        foreach ($palestras as $palestra)
        {
            $link_palestra = $this->pl->get_link_palestra($palestra['id_palestra']);
            $palestras[$i]['link_palestra'] = $link_palestra[0]['url'];
            $i++;
        }
        
        return $palestras;
    }

    private function show_minha_palestra($usuario_id_unico)
    {
        //$this->load->model("Palestra_model","pl");
        
        $palestra_usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
        
        $minha_palestra = $this->pl->get_minha_palestra($palestra_usuario[0]['id_usuario']);
        
        
        //
        
        $this->load->library("pagination");
        
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        
        $config = array();
        //
        $config['full_tag_open'] = '<ul style="margin-left:15px; margin-right: 15px" class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['attributes'] = ['class' => 'page-link'];
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Anterior&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Proxima&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="'.base_url().'inicio/'.$page.'" class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        //
        
        $config["base_url"] = base_url() . "inicio";
        $config["total_rows"] = count( $minha_palestra );
        $config["per_page"] = PER_PAGE;
        $config["uri_segment"] = 2;
        
        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();
        //
        $minha_palestra_2 = $this->pl->get_minha_palestra_2($palestra_usuario[0]['id_usuario'], $config["per_page"], $page);
        //echo '<pre>';        var_dump($minha_palestra_2);die;
        return $this->minha_palestra_cadastrada($palestra_usuario[0]['id_usuario'], $minha_palestra_2, $links);
    }
    
    private function minha_palestra_cadastrada($id_usuario, $minha_palestra, $links=null)
    {
        if( count($minha_palestra)>0 )
        {
            
            $i = 0;
            foreach ($minha_palestra as $palestra)
            {
                
                $avaliacao_palestra = $this->pl->check_avaliacao_palestra($palestra['id_palestra']);
                $quantidade_avaliacao_palestra = count($avaliacao_palestra);
                
                $pagamento = $this->pl->check_pagamento_pastestra($id_usuario, $palestra['id_palestra']);
                
                
                if(count($pagamento)>0 )
                {
                    $minha_palestra[$i]['pago'] = TRUE;
                    $minha_palestra[$i]['id_historico'] = $pagamento[0]['id_historico'];
                    
                    $cod_autenticacao = $this->gera_cod_autenticacao($id_usuario);
                    
                    $identificador_cod = $id_usuario+$pagamento[0]['id_historico'];
                    $identificador = $this->gera_identificador($identificador_cod);
                    
                    $certificado = $this->pl->certificado($id_usuario, $pagamento[0]['id_historico'], $cod_autenticacao, $identificador);
                    
                    if($certificado['status'])
                    {
                        $recuperar_arquivo_ovumdocs =  $this->recuperar_arquivo_ovumdocs($certificado['codigo_autenticacao']);
                        //var_dump($recuperar_arquivo_ovumdocs);die;
                        $minha_palestra[$i]['url_certificado'] = $recuperar_arquivo_ovumdocs['url'];
                    }
                    else
                    {
                        $id_certificado = $certificado['codigo_autenticacao']['id_certificado']; 
                        $id_historico = $pagamento[0]['id_historico'];
                        
                        $url_certificado = base_url().'gera_certificado_palestra_palestrante/'.codifica($id_certificado).'/'.codifica($id_usuario).'/'.codifica($palestra['id_palestra']).'/'.codifica($id_historico);
                        //var_dump($url_certificado);die;
                        $add_arquivo_docs = $this->add_arquivo_docs( $certificado['codigo_autenticacao']['codigo_autenticacao'], $url_certificado );
                        
                        if (isset($add_arquivo_docs['url']))
                        {
                            $minha_palestra[$i]['url_certificado'] = $add_arquivo_docs['url'];
                        }
                        else
                        {
                            $minha_palestra[$i]['url_certificado'] = '';
                        }
                    }
                }
                else
                {
                    $minha_palestra[$i]['pago'] = FALSE;
                }
                
                $link_palestra = $this->pl->get_link_palestra($palestra['id_palestra']);
                $minha_palestra[$i]['link_palestra']= $link_palestra[0]['url'];
                //$minha_palestra[$i]['pago'] = $this->pl->check_pagamento_pastestra($id_usuario, $palestra['id_palestra']);
                $minha_palestra[$i]['avaliacao'] = $quantidade_avaliacao_palestra;
                $i++;
            }
            $minha_palestra['links'] = $links;
            //echo '<pre>';var_dump($minha_palestra);            die();
            return $minha_palestra;
        }
        
        return $minha_palestra;
    }
    
    private function gera_cod_autenticacao($id_usuario)
    {
        $hash = date('Y') . '-' . strtoupper(substr(md5( $id_usuario . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5( $id_usuario . ':' . microtime()), 0, 4));
        $autenticacao = strtoupper(substr(md5($hash), 0, 8));
        
        return date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
    }
    
    private function gera_identificador($id)
    {
        $hash = date('Y') . '-' . strtoupper(substr(md5( $id . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5( $id . ':' . microtime()), 0, 4));
        $autenticacao = strtoupper(substr(md5($hash), 0, 8));
        
        return date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
    }

    private function recuperar_arquivo_ovumdocs($identificador) 
    {

        $url =  DOCS_URL . 'link';

        #Chaves de acesso à API enviadas via POST, junto com URL de acesso ao arquivo, Id do Arquivo, e Descrição opcional
        $post = array(
            'chave'        => DOCS_CHAVE,
            'token'        => DOCS_TOKEN,
            'id_arquivo' => $identificador
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);


        #Captura Resposta de OvumDocs
        $requisicao = curl_exec($ch);#$requisição é um Objeto JSON
        $resposta = json_decode($requisicao, TRUE);#$requisição é um Objeto JSON
        curl_close($ch);

    //        var_dump($resposta);
        #Exibição em tela do array de resposta
        return $resposta;
  }

    private function add_arquivo_docs($id_arquivo, $url_certificado)
    {
        $url = DOCS_URL.'salvar';
        
        $post = array(
        'chave' => DOCS_CHAVE,
        'token' => DOCS_TOKEN,
        'url_arquivo' => $url_certificado,
        'id_arquivo' => $id_arquivo,
        'descricao' => ''
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        $requisicao = curl_exec($ch);
        $resposta = json_decode($requisicao, TRUE);
        curl_close($ch);
        
        return $resposta;
    }

    public function url_retorno()
    {
        //$response_codificada = '{"status":2,"info":"Altera\u00e7\u00f5es nas informa\u00e7\u00f5es do v\u00eddeo.","code":"SUCCESS","erro":null,"video":{"id_video":"100","id_usuario":"danin","id_instituicao":"2","titulo":"Modelagem de Dados 14","descricao":"Modelagem de Dados 14","link":"https:\/\/www.aporfia.com.br\/ovumagenda\/go\/colloquium\/video\/x8t3o","identificador":"x8t3o","url_retorno":"https:\/\/www.aporfia.com.br\/palestra\/url_retorno","disponibilidade":"1","classificacao_indicativa":"L","duracao":"0:14","imagem":"https:\/\/i.ytimg.com\/vi\/ETngtCXaInw\/hqdefault.jpg","status":"1","tags":["MODELAGEM","DADOS"]}}';
        //$response_codificada = json_decode($response_codificada, true);
       //echo '<pre>';var_dump($response);die;
        $response_codificada = file_get_contents("php://input");
        $response = json_decode($response_codificada, true);
        //$response = "{'situacao':'1'}";
        $this->load->model("Palestra_model","pl");
        
        if( isset($response['status']) && ($response['status']==2) )
        {
            $identificador = $response['video']['identificador'];
            
            $id_palestra = $this->pl->consulta_palestra_identificador($identificador);
            
            if( count($id_palestra)>0)
            {
                $situacao = $response['video']['disponibilidade'];
                
                if($situacao==0)
                {
                    $situacao_palestra = 0;
                }
                else if($situacao==1)
                {
                    $situacao_palestra = 2;
                }
                else if($situacao==2)
                {
                    $situacao_palestra = 1;
                }
                elseif ($situacao==3)
                {
                    $situacao_palestra = 0;
                }  
                
                $this->pl->edit_situacao_palestra($id_palestra[0]['id_palestra'], $situacao_palestra);
                
            }
            
            
        }
        $this->pl->termo_uso($response_codificada);
        
    }

    public function send()
    {
        //echo json_encode(['status'=>6]);die;
        //echo count( $_POST['video'] );
        echo '<br>';
        echo '<pre>';var_dump( $_POST['video'] );
        echo '<br>';
        echo '<pre>';var_dump($_FILES['file']);
        die();
        
        /*  
        //$type_extension = ['video/rm','video/VOB','video/x-matroska','video/ogv','video/Ogg','video/webm','video/mp4','video/mpeg2','video/mpeg4', 'video/mpegps','video/3gp','video/3gpp','video/mov','video/FLV','video/m3u8','video/ts','video/AVI','video/ASF'];
         
        $filetype = $_FILES['arquivo']['type'];
        $type_video = strstr($filetype, '/', true);
        
        //echo mime_content_type($_FILES['arquivo']['tmp_name']);
        //;
        
        if ( $type_video=='video' )
        {
            echo $type_video;
        }
        else
        {
            echo 'Falha';
        }
        die;*/
        //echo '<pre>';var_dump($this->input->post());die;
        
        if( ( !empty($_POST['video']['titulo']) && !empty($_POST['video']['tags']) && !empty($_POST['video']['descricao'])  ) )
        {
        
            $this->load->library('form_validation');

            $this->form_validation->set_rules('video[titulo]', 'video[titulo]', 'trim|required');
            $this->form_validation->set_rules('video[descricao]', 'video[descricao]', 'trim|required|max_length[250]');
            $this->form_validation->set_rules('video[tags]', 'video[tags]', 'trim|required');

            if (empty($_FILES['file']['tmp_name']))
            {
                $this->form_validation->set_rules('file', 'file', 'required');
            }

            //$inipath = php_ini_loaded_file();
            //echo '<pre>';var_dump( $_REQUEST );
            //echo '<pre>';var_dump($_FILES);die;
            //echo '<pre>';var_dump($_FILES);DIE;

            if( ( isset($_FILES['file']['tmp_name']) ) && ($_FILES['file']['error']===0) && (is_uploaded_file($_FILES['file']['tmp_name'])===TRUE) )
            {

            //echo $_FILES['arquivo']['tmp_name'];
            //var_dump($this->form_validation->run());die;
                
                if($_FILES['file']['size']<='5000000')
                {
                    
                
                
                if ($this->form_validation->run()) 
                {
                    
                    //$filetype = $_FILES['arquivo']['type'];
                    //$type_extension = ['video/rm','video/ogv','video/webm','video/mp4','video/mpeg2','video/mpeg4', 'video/mpegps','video/3gp','video/3gpp','video/mkv','video/mov','video/flv','video/m3u8','video/ts','video/avi','video/wmv'];
                     
                    $filetype = $_FILES['file']['type'];
                    $type_video = strstr($filetype, '/', true);
                    
                    if ( $type_video=='video' )
                    {

                        $usuario_id_unico = $this->session->userdata('usuario_id_unico');
                        $this->load->model("Palestra_model", "pl");
                        $usuario = $this->pl->get_usuario_id_unico($usuario_id_unico);
                        //echo '<pre>';var_dump($usuario);die;
                        //$dados_form = $this->input->post();

                        $titulo = $this->input->post("video[titulo]", TRUE);
                        $titulo = filter_var($titulo, FILTER_SANITIZE_STRING);

                        $descricao = $this->input->post("video[descricao]", TRUE);
                        $descricao = filter_var($descricao, FILTER_SANITIZE_STRING);
                        
                        //video[tags]
                        $tags = $this->input->post("video[tags]", TRUE);
                        $tags = filter_var($tags, FILTER_SANITIZE_STRING);

                        $classificacao_indicativa = $this->input->post("video[classificacao_indicativa]", TRUE);
                        $classificacao_indicativa = filter_var($classificacao_indicativa, FILTER_SANITIZE_STRING);

                        /*$data = [
                                        'video'=>[
                                                'titulo'=>$titulo,
                                                 'tags'=>$tags,
                                                 //'classificacao_indicativa'=>$classificacao_indicativa,
                                                 'descricao'=>$descricao
                                                ]
                                      ];*/

                        //$data['auth'] = ['chave' => INSTITUICAO_CHAVE, 'token' => INSTITUICAO_TOKEN, 'user' => $usuario[0]['id_unico'] ];


                        #URL para Função de Upload de vídeo
                        $url = COLLOQUIUM_URL_SALVAR;

                        $path_file = $_FILES['file']['tmp_name']; # caminho real do arquivo de video
                        # Autenticação
                        $data["auth[chave]"] = INSTITUICAO_CHAVE;
                        $data["auth[token]"] = INSTITUICAO_TOKEN;
                        $data["auth[user]"] = $usuario[0]['id_unico'];

                        # Informações para o vídeo
                        $data["video[titulo]"] = $titulo;
                        $data["video[descricao]"] = $descricao;
                        $data["video[tags]"] = $tags;

                        $data["video[classificacao_indicativa]"] = $classificacao_indicativa;
                        $data["video[url_retorno]"] = base_url() . 'url_retorno';

                        # Arquivo de vídeo para upload
                        $data["video_arquivo"] = curl_file_create(
                                $path_file, mime_content_type($path_file)
                        );
                        
                        //echo '<pre>';var_dump($data);die;

                        //echo '<pre>';var_dump($data);die;
                        # Envio para Colloquium
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        # Resposta de API Colloquium (Objeto JSON convertido em Array)
                        //$JSON = curl_exec($ch);
                        //echo '<pre>';var_dump($JSON);die;
                        $response = json_decode(curl_exec($ch), true);

                        # Resposta do servidor (código de estado HTTP )
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        curl_close($ch);
                        //echo 'httpcode: ' . $httpCode . '<pre>';
                        if ($httpCode == 200)
                        {
                            //$code = json_encode($response);
                            //$this->pl->termo_uso($code);

                            if ($response['status'] == 1)
                            {

                                $id_usuario = $usuario[0]['id_usuario'];

                                //$location = $this->location();
                                $location = 'On-line';
                                $id_palestra = $this->pl->add_palestra($id_usuario, $response['video']['titulo'], $response['video']['descricao'], $location);
                                $id_modulo = $this->pl->add_modulo();

                                if (!empty($id_palestra) && !empty($id_modulo)) {
                                    $this->pl->add_modulo_palestra($id_modulo, $id_palestra);

                                    $id_material = $this->pl->add_material($response['video']['identificador'], $response['video']['link']);

                                    if (!empty($id_material))
                                    {
                                        $add_material_modulo = $this->pl->add_material_modulo($id_material, $id_modulo);

                                        if($add_material_modulo)
                                        {

                                        }

                                        $this->session->set_flashdata('feedback_request', ' Parabéns! Palestra Cadastrada com sucesso, sua Palestra esta sob análise, aguarde o deferimento de sua Palestra');
                                        echo json_encode(['status'=>1]);
                                        //redirect("inicio");
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                                else
                                {
                                    
                                }
                            } 
                            elseif ($response['status'] == 0) 
                            {
                                $this->session->set_flashdata('feedback_request', 'Erro na submissão da Palestra');
                                echo json_encode(['status'=>2]);
                                //redirect("pagina_cadastro_palestra");
                            }
                            else
                            {
                                $this->session->set_flashdata('feedback_request', 'Problema na operação');
                                echo json_encode(['status'=>3]);
                                //redirect("pagina_cadastro_palestra");
                            }
                        } 
                        else
                        {
                            $this->session->set_flashdata('feedback_request', 'Falha na requisição');
                            echo json_encode(['status'=>4]);
                            //redirect("pagina_cadastro_palestra");
                        }
                    
                    }
                    else 
                    {
                        $this->session->set_flashdata('feedback_request', 'É necessario enviar Palestra no formato de Vídeo');
                            echo json_encode(['status'=>5]);
                    }
                    
                } 
                else
                {
                    $this->session->set_flashdata('feedback_request', 'Dados Inválidos');
                    echo json_encode(['status'=>6]);
                    //redirect("pagina_cadastro_palestra");
                }
                
                //
            }
            else
            {
                $this->session->set_flashdata('feedback_request', 'Arquivo de vídeo acima do tamanho Permitido (5MB)');
                echo json_encode(['status'=>7]);
            }

            }
            else
            {
                $this->session->set_flashdata('feedback_request', 'Método incorreto para upload do arquivo');
                echo json_encode(['status'=>8]);
                //redirect("pagina_cadastro_palestra");
            }
        
        }
        else
        {
         $this->session->set_flashdata('feedback_request', 'Dados Vazios');
            echo json_encode(['status'=>9]);   
        }
        
    }
    
    private function location()
    {
        
        $ip = $this->get_client_ip(); 
        if($ip=='UNKNOWN')
        {
            return $ip;
        }
        else
        {
            $json  = file_get_contents("http://ipinfo.io/$ip/geo");
            $json  =  json_decode($json ,true);
            //var_dump($json);die;
            if( isset($json['country']) && isset($json['region']) && isset($json['city']) )
            {
                $country =  $json['country'];
                $region= $json['region'];
                $city = $json['city'];
                return $city."/".$region."/".$country;
            }
            else
            {
                return "UNKNOWN";
            }
        }
    }
    

    private function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    public function insert_usuario_termo_uso($dados)
    {
        
        $dados  = [
                   'id_usuario'=> $dados['id_usuario'],
                   'id_termo_uso'=> $dados['id_termo_uso'] 
                  ];
        
        $is_aceite = $this->pl->insert_usuario_termo_uso($dados);
        
        if($is_aceite)
        {
            $this->index();
            //$this->load->view('site/paginas/index');
        }
    }
    
    public function do_aceite()
    {
        $this->load->model("Palestra_model","pl");
        
        $dados_aceite = $this->input->post();
        //var_dump($dados_aceite);die;
        $request =  decodifica( $dados_aceite['request'] );
        $request = json_decode($request,TRUE);
       
        $id_instituicao =  decodifica($dados_aceite['id_instituicao']);
        
        $id_termo_uso = decodifica($dados_aceite['termo_uso']);
        $id_usuario = $this->pl->usuario($request);
        $this->pl->instituicao_usuario($id_usuario, $id_instituicao);
        
        $dados_termo_uso = [
                            'id_usuario'=> $id_usuario,
                            'id_termo_uso'=> $id_termo_uso
                           ];
        $this->insert_usuario_termo_uso($dados_termo_uso);
    }

    private function erro($data=null)
    {
        $this->load->view('site/paginas/error', $data);
    }
    
    public function codifica_dados_enviados()
    {
        $response = file_get_contents("php://input");
        $response_security = $this->security->xss_clean($response);
        //echo '<pre>';var_dump($response_security);die;
        
        $response_decode = json_decode($response_security, TRUE);
        
        if( isset($response_decode['autenticacao']['chave']) && isset($response_decode['autenticacao']['token']) && isset($response_decode['usuario']['id_unico']) && !empty($response_decode['autenticacao']['chave']) && !empty($response_decode['autenticacao']['token']) && !empty($response_decode['usuario']['id_unico']) )
        {
            $codifica_response = codifica($response_security);
            //var_dump($codifica_response);die;
            echo $codifica_response;
        }
        else
        {
            
            $not_found = json_encode(['404'=>'404']);
            echo codifica($not_found);   
        }
        
    }
    
    public function not_found()
    {
        $this->erro(['mensagem'=>'Falha ao Enviar Dados para Codificação' ]);
    }

    public function consulta($response)
    {
        //$response = file_get_contents("php://input");
        $response_new = $this->security->xss_clean($response);
        $response = decodifica($response_new);
        //var_dump($response);die;
        $request = json_decode( $response, true );
        
        //($request);die;
        if( isset($request['404']) )
        {
            $this->not_found();
            die;
        }
        
       if( (count($response)>1) || ($response!=''))
        {
            $this->check_estrutura($request, 'autenticacao_centralizadora');
            $this->check_dados($request, 'autenticacao_centralizadora');

            $this->load->model("Palestra_model","pl");
            
            $response_autenticacao = $this->autentica_instituicao($request['autenticacao']['chave'], $request['autenticacao']['token']);
            //var_dump($response_autenticacao);die;

            if( ( isset($response_autenticacao['situacao']) ) && ($response_autenticacao['situacao']=='Sucesso') )
            {
                $id_instituicao = $this->pl->instituicao($response_autenticacao);
                //var_dump($id_instituicao);die;
                $this->check_estrutura($request, 'dados_aluno');
//                //echo '<pre>';var_dump($request);die;
                $this->check_dados($request, 'dados_aluno');
                
                $this->usuario($request, $id_instituicao);
            }
            else
            {
                $this->erro(['mensagem'=>$response_autenticacao['mensagem'] ]);
                //echo json_encode( $response_autenticacao['mensagem'], JSON_FORCE_OBJECT );
            }
        }
        else
        {
            $data = ['mensagem'=>'Dados vazios'];
             $this->erro($data);
        }

    }
    
    private function usuario($request, $id_instituicao)
    {
        
        $dados_aluno = $this->pl->consulta_usuario($request);
        //echo '<pre>';var_dump($request);die;
        $id_usuario = ( isset($dados_aluno[0]['id_usuario']) ) ? $dados_aluno[0]['id_usuario'] : "";
        $is_usuario_termo_uso = $this->pl->usuario_termo_uso($id_usuario);
        
        if(!$is_usuario_termo_uso)
        {
            $termo_uso_ativo = $this->pl->termo_uso_ativo();
            $data = [
                    'request'=> json_encode($request), 
                    'id_instituicao'=>$id_instituicao,
                    'termo_uso_ativo'=>$termo_uso_ativo
                    ];
            
            $this->load->view('site/paginas/termo_uso', $data);
        }
        else
        {
            $session = array(
                        'usuario_id_unico' => (!isset($request['usuario']['id_unico'])) ? '' : $request['usuario']['id_unico'] ,
                        'usuario_nome' => (!isset($request['usuario']['nome'])) ? '' : $request['usuario']['nome'],
                        'usuario_email' => (!isset($request['usuario']['email'])) ? '' : $request['usuario']['email'],
                        'usuario_documento' => (!isset($request['usuario']['documento'])) ? '' : $request['usuario']['documento'],
                        'usuario_logado' => TRUE
                    );
            $this->session->set_userdata($session);
            
            $this->index();
        }
    }

        private function show_mensagem_dados( string $tipo)
        {
            switch ($tipo):
                case 'autenticacao_centralizadora':
                    $data = ['mensagem'=>'E necessario definir corretamente tipo e valores de autenticacao para chave, token e id_instituicao'];
                    $this->erro($data);
                    exit();
                    break;
                case 'dados_aluno':
                    $data = ['mensagem'=>'E necessario definir corretamente tipo e valores de autenticacao para o usuario'];
                    $this->erro($data);
                    exit();
                    break;
                default:
                    $data = ['mensagem'=>'Tipo não definido para mensagem'];
                    $this->erro($data);
                    exit();
            endswitch;
            
        }
        
        private function show_mensagem_estrutura( string $tipo)
        {
            switch ($tipo):
                case 'autenticacao_centralizadora':
                    $data = ['mensagem'=>'E necessario definir corretamente as chaves para autenticacao'];
                    $this->erro($data);
                    break;
                case 'dados_aluno':
                    $data = ['mensagem'=>'E necessario definir corretamente as chaves para o usuario'];
                    $this->erro($data);
                    break;
                default:
                    $data = ['mensagem'=>'Tipo não definido para mensagem'];
                    $this->erro($data);
            endswitch;
            
        }

        private function check_estrutura( $dados, string $tipo)
        {
            switch ($tipo):
                case 'autenticacao_centralizadora':
                    if(array_key_exists('autenticacao', $dados))
                    {
                        $dados = $dados['autenticacao'];
                        if( !array_key_exists('chave', $dados) || !array_key_exists('token', $dados) )
                        {
                            $this->show_mensagem_estrutura($tipo);
                        }
                    }
                    else
                    {
                        $this->show_mensagem_estrutura($tipo);
                    }
                    break;
                case 'dados_aluno':
                    if(array_key_exists('usuario', $dados))
                    {
                        $dados = $dados['usuario'];
                        if( !array_key_exists('id_unico', $dados) || !array_key_exists('nome', $dados) || !array_key_exists('email', $dados) || !array_key_exists('documento', $dados) )
                        {
                            $this->show_mensagem_estrutura( $tipo);
                        }
                    }
                    else
                    {
                        echo json_encode( ['status'=>0,'Mensagem'=>'E necessario definir corretamente a chave usuario'], JSON_FORCE_OBJECT );
                        exit;
                    }
                    break;
                default:
                    echo "Tipo de estrutura não definida";
            endswitch;
        }
        
        private function check_dados( array $dados, string $tipo)
        {
            
            switch ($tipo):
                case 'autenticacao_centralizadora':
                    
                    $chave = filter_var( $dados['autenticacao']['chave'], FILTER_SANITIZE_STRING);
                    $token = filter_var ( $dados['autenticacao']['token'], FILTER_SANITIZE_STRING);
                    
                    if( !is_string($dados['autenticacao']['chave']) || !is_string($dados['autenticacao']['token']) )
                    {
                        $this->show_mensagem_dados($tipo);
                        
                        if( empty( trim($chave) ) || empty( trim($token) ) )
                        {
                            $this->show_mensagem_dados($tipo);
                        }
                    }
                    break;
                case 'dados_aluno':
                    $id_unico = filter_var($dados['usuario']['id_unico'], FILTER_SANITIZE_STRING);
                    $nome = filter_var($dados['usuario']['nome'], FILTER_SANITIZE_STRING);
                    $email = filter_var($dados['usuario']['email'], FILTER_SANITIZE_STRING);
                    $documento = filter_var($dados['usuario']['documento'], FILTER_SANITIZE_STRING);
                    
                    if( !is_string($dados['usuario']['id_unico']) )
                    {
                        $this->show_mensagem_dados($tipo);
                    }
                    elseif ( empty( trim($id_unico) ) )
                    {
                        $this->show_mensagem_dados($tipo);
                    }
                    break;
                default:
                    echo "Tipo de estrutura não definida";
            endswitch;
            
        }

        public function autentica_instituicao($chave=null, $token=null, $id_instituicao=null)
        {
            if( !empty( trim($chave) ) && !empty( trim($token) ) )
            {
                $url = INSTITUICAO_URL;
                $data["autenticacao"]["chave"] = $chave;
                $data["autenticacao"]["token"] = $token;
                $data["autenticacao"]["servico"] = INSTITUICAO_SERVICO;

                $data = json_encode($data);
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                if($httpCode==200)
                {
                    $autenticacao = json_decode( $result, true);
                    //echo '<pre>';var_dump($autenticacao);die;
                    //$cor = $autenticacao['cor_instituicao'];
                    //$this->session->set_userdata('cor_instituicao', $cor);
                    return $autenticacao;
                }
                else
                {
                    echo 'Falha na Comunicação';
                }

                curl_close($ch);
            }
            else
            {
                echo json_encode( ['Mensagem'=>'Parametros obrigatorios de autenticacao vazio'], JSON_FORCE_OBJECT );
                exit();
            }
        }
}