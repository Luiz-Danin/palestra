<?php $this->load->view('aluno/includes/head.php'); ?>
<!--<link rel="stylesheet" href="public/css/tokenfield-typeahead.css">
<link rel="stylesheet" href="public/css/bootstrap-tokenfield.css">

<script src="public/js/bootstrap-tokenfield.js"></script>
<script src="public/js/typeahead.bundle.min.js"></script>
<script src="public/js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="public/css/jquery-ui.min.css">-->


<style>

section {
    padding: 60px 0;
}

section .section-title {
    text-align: center;
    color: #007b5e;
    margin-bottom: 50px;
    text-transform: uppercase;
}
#tabs{
	background: #fff;
    color: #eee;
}
#tabs h6.section-title{
    color: #212529;
}

#tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #f3f3f3;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 4px solid !important;
    font-size: 20px;
    font-weight: bold;
}
#tabs .nav-tabs .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #212529;
    font-size: 20px;
}

label
{
    color:#000;
}

.tab-content>.active 
{
    color:#000;
}
    
/*upload*/

.picture-container{
    position: relative;
    cursor: pointer;
    text-align: center;
    margin-bottom: 15px;
}

.picture{
    width: 100px;
    height: 100px;
    background-color: #fff;
    color: #FFFFFF;
    margin: 5px auto;
    overflow: hidden;
    -webkit-transition: all 0.2s;
    -moz-transition: all 0.2s;
    -o-transition: all 0.2s;
    transition: all 0.2s;
}

.picture > .icon{
    width: 100%;
    height: 100%;
    display: inline-block;
    color: #37474F;
    border: 4px solid #CCCCCC;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
    -webkit-transition: all 0.2s;
    -moz-transition: all 0.2s;
    -o-transition: all 0.2s;
    transition: all 0.2s;
}

.picture:hover .icon{
    border-color: greenyellow;
}

.picture > .icon > svg{
    height: 1.4em;
    font-size: 4em;
}

.picture > svg{
    width: 100%;
    height: 100%;
}

.picture input[type="file"]{
    cursor: pointer;
    display: block;
    height: 100%;
    left: 0;
    opacity: 0 !important;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 100;
    right: 0;
    bottom: 0;
}

/*svg tick animation*/
.circ {
    opacity: 0;
    display: none;
    stroke-dasharray: 130;
    stroke-dashoffset: 130;
    -webkit-transition: all .75s;
    -moz-transition: all .75s;
    -ms-transition: all .75s;
    -o-transition: all .75s;
    transition: all .75s;
}
.tick{
    stroke-dasharray: 50;
    stroke-dashoffset: 50;
    -webkit-transition: stroke-dashoffset .4s 0.5s ease-out;
    -moz-transition: stroke-dashoffset .4s 0.5s ease-out;
    -ms-transition: stroke-dashoffset .4s 0.5s ease-out;
    -o-transition: stroke-dashoffset .4s 0.5s ease-out;
    transition: stroke-dashoffset .4s 0.5s ease-out;
}
.drawn > svg .path{
    display: block;
    opacity: 1;
    stroke-dashoffset: 0;
}

.drawn{
    border-color: #fff;
}

[data-toggle="popover"]{
    cursor: pointer;
}

span.popover-content-remove {
    padding-left: 10px;
    color: red;
    cursor: pointer;
    float: right;
}

.pb10{
    padding-bottom: 10px;
}

.popover-header{
    text-align: center;
}

.popover{
    min-width: 200px;
}
</style>
<section id="tabs">
	<div class="container-fluid">
		<!--<h6 class="section-title h1">Palestras</h6>-->
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Cadastrar Palestras</a>
						<!--<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Cadastrar Palestras</a>-->
						<!--<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Palestras Disponíveis</a>-->
						<!--<a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">About</a>-->
					</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                        <?php
                                        if (isset($_SESSION['feedback_request'])) {
                                            ?>
                                        
                                        <div class="container">
                                            <div class="alert alert-info">
                                              <?= $_SESSION['feedback_request'] ?>.
                                            </div>
                                          </div>

                                            <?php
                                                unset($_SESSION['feedback_request']);
                                            }
                                        ?>
<!--                                        <div class="container-fluid">
                                            
                                            <form method="post" enctype="multipart/form-data" action="<?php echo base_url();?>send">
                                                <input value="<?php echo $id_usuario;?>" type="hidden" name="usuario">
                                                
                                                <div class="form-group">
                                                    <label for="title">Titulo da Palestra:</label>
                                                    <input class="form form-control"  type="text" name="video[titulo]" placeholder="Digite o Título de sua Palestra" maxlength="100" required/>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="description">Descrição da Palestra:</label>
                                                    <textarea required class="form form-control"  name="video[descricao]" placeholder="Digite a Descrição de sua Palestra" cols="20" rows="1" maxlength="5000" ></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tags">Tags dda Palestra: [ separa por ';' ]</label>
                                                    <input required class="form form-control"  type="text" placeholder="Digite as Tags (separadas por ;)" name="video[tags]" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="tags">Classificação indicativa: (Opcional)</label>
                                                    <input class="form form-control-file" type="text" placeholder="Valores aceitos: L, 10, 12, 14, 16, ou 18. Caso fique em branco será considerado ‘L’: livre para todos os públicos" name="video[classificacao_indicativa]" >
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="file">Arquivo de Video:</label> 
                                                    <input required  class="form form-control" type="file" name="arquivo" >
                                                </div>
                                                <button class="btn btn-primary btn-block" type="submit">Enviar Palestra <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                            </form>
                                        </div>-->
                                        
                                        <!--novo form-->
                                        <div class="container-fluid">
                                            <!--<h2 class="text-center">Contac Form</h2>-->
                                                <div class="row justify-content-center">
                                                        <div class="col-12 col-md-12 col-lg-21 pb-5">
                                                            

                                                            <!--Form with header-->
                                                            <!--https://bootsnipp.com/snippets/35VBm-->
                                                            <!--<form id='imageuploadform'  method="post" enctype="multipart/form-data" action="<?php //echo base_url();?>send">-->
                                                            <form  id='imageuploadform'  method="post" enctype="multipart/form-data">
                                                                <div class="card border-dark rounded-0">
                                                                    <div class="card-header p-0">
                                                                        <div class="bg-secondary text-white text-center py-2">
                                                                            <h3><i class="fas fa-file-video" aria-hidden="true"></i> Envie sua Palestra no Formato de Vídeo</h3>
                                                                            <!--<p class="m-0">Con gusto te ayudaremos</p>-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="card-body p-3">

                                                                        <!--Body-->
                                                                        <div class="form-group">
                                                                            <div class="input-group mb-2">
                                                                                <div class="input-group-prepend">
                                                                                    <div class="input-group-text"><i class="fa fa-text-width text-dark"></i></div>
                                                                                </div>
                                                                                <input id="titulo" type="text" class="form form-control" id="titulo" name="video[titulo]" placeholder="Nome de sua Palestra" maxlength="100" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="input-group mb-2">
                                                                                <div class="input-group-prepend">
                                                                                    <div class="input-group-text"><i class="fa fa-tags text-dark"></i></div>
                                                                                </div>
                                                                                <input id="tags" type="text" class="form form-control" id="tags" name="video[tags]" placeholder="Palavra-chave separadas por ; (ponto e vígula)" required>
                                                                                <!--<input type="text" class="form-control" id="tokenfield" name="video[tags]" placeholder="Palavra-chave (Tecle Enter ao teminar a palavra)" required>-->
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        <div class="form-group">
                                                                            <div class="input-group mb-2">
                                                                                <div class="input-group-prepend">
                                                                                    <div class="input-group-text"><i class="fa fa-universal-access  text-dark"></i></div>
                                                                                </div>
                                                                                
                                                                                <select id="classificacao" class="form-control" name="video[classificacao_indicativa]">
                                                                                    <option selected value="">Selecione a Classificação Indicativa (Opcional)</option>
                                                                                    <option value="L">Livre para todos os públicos</option>
                                                                                    <option value="18">Para maiores de 18 anos</option>
                                                                                    <!--<option value="16">Para menores de 18 anos</option>-->
                                                                                    </select>
                                                                                    <!--<input id="classificacao" type="text" class="form form-control" id="tags" name="video[classificacao_indicativa]" placeholder="Classificação indicativa (Opcional). Valores aceitos: L(livre para todos os públicos), 10, 12, 14, 16, ou 18">-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="input-group mb-2">
                                                                                <div class="input-group-prepend">
                                                                                    <div class="input-group-text"><i class="fa fa-comment text-dark"></i></div>
                                                                                </div>
                                                                                <textarea id="descricao" name="video[descricao]" class="form-control" placeholder="Descrição da Palestra (Máximo de 250 Caracteres)" maxlength="250" required></textarea>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        <!--upload-->
                                                                        
                                                                        <div class="custom-file-picker"> 
                                                                            <div class="picture-container form-group">
                                                                                <h4 class="info_text">Selecione seu Arquivo de Vídeo(até 5MB)</h4>
                                                                                <div class="picture form-group">
                                                                                    <span class="icon"><i class="fa fa-file-video"></i></span>
                                                                                    <input id="arquivo" required type="file" accept="video/*" class="wizard-file form form-control" id="a8755cf0-f4d1-6376-ee21-a6defd1e7c08">
                                                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 37 37" xml:space="preserve">
                                                                                        <path class="circ path" style="fill:none;stroke:#77d27b;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:10;" d="M30.5,6.5L30.5,6.5c6.6,6.6,6.6,17.4,0,24l0,0c-6.6,6.6-17.4,6.6-24,0l0,0c-6.6-6.6-6.6-17.4,0-24l0,0C13.1-0.2,23.9-0.2,30.5,6.5z"></path>
                                                                                        <polyline class="tick path" style="fill:none;stroke:#77d27b;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:10;" points="11.6,20 15.9,24.2 26.4,13.8 "></polyline>
                                                                                    </svg>
                                                                                </div>     
                                                                            </div>
<!--                                                                            <div class="popover-container text-center">
                                                                                <p data-toggle="popover" data-id="a8755cf0-f4d1-6376-ee21-a6defd1e7c08" class="btn-popover" data-original-title="" title="">
                                                                                    <span class="file-total-viewer">0</span> Video Selecionado <input type="button" value="Visualizar" href="javascript:void(0)" class="btn btn-success btn-xs btn-file-view">
                                                                                </p>
                                                                            </div>-->
                                                                        </div>
                                                                        
                                                                        <!--upload-->

                                                                        <div class="text-center">
                                                                            <!--<input type="submit" value="Enviar Palestra" class="btn btn-secondary btn-block rounded-0 py-2">-->
                                                                            <!--<div class="upload"></div>-->
                                                                                <button id="btnupload" class="btn btn-outline-dark btn-block" type="submit">Enviar Palestra <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </form>
                                                            <!--Form with header-->


                                                        </div>
                                                </div>
                                        </div>
                                        <!--novo form-->

                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
<!--                                        -->
                                    </div>
					<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
						
                                            
                                        </div>
					</div>
<!--					<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
						Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
					</div>-->
				</div>
			
			</div>
		</div>
	</div>
</section>

<script>
/*$(document).ready(function() {   
    $('#tokenfield').tokenfield({
      autocomplete: {
        
        delay: 100
      },
      showAutocompleteOnFocus: true,
      delimiter	:';'
    });
    
});*/
</script>

<script>
//if($('#arquivo').val() != '') {

    $("#arquivo").on("change", function(){
        
           //alert('This file size is: ' + this.files[0].size/1024/1024 + "MB");
      $.each($('#arquivo').prop("files"), function(k,v){
          
          
          
          var filename = v['name'];    
          var ext = filename.split('.').pop().toLowerCase();
            if($.inArray(ext, ['','rm','ogv','webm','mp4','mpeg4', 'mpegps','3gp','3gpp','mkv','mov','flv','m3u8','ts','avi','wmv']) == -1) {
              alert('Por favor, envie apenas arquivos em formato de Vídeo');
              document.location.href = document.location.href;
              //return false;
          }
      });  
      
      });
//}
</script>
<script>
$("#btnupload").click(function(e) {

    //$("#btnupload").click();
    //e.preventDefault();

});


//$('#fileupload').change(function (e) {
//
//    $("#imageuploadform").submit();
//    e.preventDefault();
//
//});
  
    
$('#imageuploadform').submit(function(e) {

    
    var file_data = $('#arquivo').prop('files')[0];   
    //var form_data = new FormData(this);
    var form_data = new FormData(this);                  
    form_data.append('file', file_data);
    
    var base_url = "<?php echo base_url(); ?>";
    
    //var titulo = $("#titulo").val();
    //var tags = $("#tags").val();
    //var classificacao = $("#descricao").val();
    //var titulo = $("#titulo").val();
    //var arquivo = $("#arquivo").val();

    //console.log(form_data);
    
    $.ajax({
        type:'POST',
        url: base_url+'send',
        //dataType: 'text',
        beforeSend: function(){
            
            $('#btnupload').prop('disabled', true);
            $('#titulo').prop('disabled', true);
            $('#tags').prop('disabled', true);
            $('#tokenfield').prop('disabled', true);
            $('#tokenfield-tokenfield').prop('disabled', true);
            $('#descricao').prop('disabled', true);
            $('#arquivo').prop('disabled', true);
            $('#classificacao').prop('disabled', true);

        } ,
        //data:{form_data, titulo:titulo, tags:tags, classificacao:classificacao, arquivo:arquivo, file_data:file_data},
        data: form_data,
        xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress',progress, false);
                }
                
                return myXhr;
        },
        cache:false,
        contentType: false,
        processData: false,

        success:function(data){
            
            var response = JSON.parse(data);
            //console.log(response.status);
            
            if( response.status==1)
            {
                document.location.href = base_url+"inicio";
            }
            else
            {
                document.location.href = base_url+"pagina_cadastro_palestra";
            }
                
          //alert('data returned successfully');

        },

        error: function(data){
            console.log(data);
        }
    });
    e.preventDefault();

});


function progress(e){

    if(e.lengthComputable){
        var max = e.total;
        var current = e.loaded;

        var Percentage = (current * 100)/max;
        console.log(Percentage);
        var percent = parseFloat(Math.round(Percentage * 100) / 100).toFixed(0);

        $("#btnupload").text("Enviando Palestra..."+percent+"% Completo");
        //$("#btnupload").hide();

        //$("#upload").html('<button id="btnupload" class="btn btn-outline-dark btn-block" type="submit"><div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated progress-bar bg-dark" style="width:'+percent+'%">'+percent+'% Completo</div></div></button>');
        //<button id="btnupload" class="btn btn-outline-dark btn-block" type="submit">Enviar Palestra <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
        if(Percentage >= 100)
        {
            //$("#btnupload").hide();
            //$("#btnupload").show();
            $("#btnupload").text("Aguarde...");
           // process completed  
        }
    }  
 }

</script>
<script>
    
    /*
    var base_url = "<?php echo base_url(); ?>";
$.ajax({
    url: base_url+'send',
    type: 'post',
    data: {payload: payload},
    xhr: function () {
        var xhr = $.ajaxSettings.xhr();
        xhr.onprogress = function e() {
            // For downloads
            if (e.lengthComputable) {
                console.log(e.loaded / e.total);
            }
        };
        xhr.upload.onprogress = function (e) {
            // For uploads
            if (e.lengthComputable) {
                console.log(e.loaded / e.total);
            }
        };
        return xhr;
    }
}).done(function (e) {
    // Do something
}).fail(function (e) {
    // Do something
});
*/
</script>

<script>
// Add the following code if you want the name of the file appear on select
/*$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});*/
</script>

<script>
$('.btn.btn-primary.btn-block').prop('disabled', true);



//Global object to store the files
let fileStorage = {};

$(document).ready(function(){
   //Handle the file change
   $("input[type='file']").change(function(e){
      //Get the id
      let id = e.target.id;

      //Get the files
      let files = e.target.files;

      //Store the file
      storeFile(id, files);

      //Show the complete icon
      $(this).siblings('.icon').hide();
      $(this).parent().removeClass('drawn');
      setTimeout(() => {
         $(this).parent().addClass('drawn');
      }, 50); 
   });
  
    //Store the file for particular filepicker
    let storeFile = (id, files) => {
      fileStorage[id] = files;

      //Update the file count
      $(`[data-id="${id}"] > .file-total-viewer`).text(files.length);
    }
    
    //Show file list
    $('[data-toggle="popover"]').popover({
        html: true,
        title: "Vídeo",
        placement:"bottom",
        content: function () {
          //Get the id of the file picker
          let id = $(this).attr('data-id');

          //Get all the files of this filepicker
          let items = fileStorage[id];

          //Preview the file 
          let template = '<div class="row">';
          if(items && items.length){
            for(let val of items){
               template += "<div class='col-12 pb10'><span class='popover-content-file-name'>" + val.name + "</span><span class='popover-content-remove' data-target='" + id + "' data-name='" + val.name + "' data-type='upload'><i class='fas fa-trash'></i></span></div>"
             }
           }else{
             template += "<div class='col-12 pb10'><span class='popover-content'>Nenhum Vídeo</span></div>";
           }

           template += '</div>';
           return template;
         }
    });
  
   //Prevent multiple popover
   $('body').on('click', function (e) {
       $('[data-toggle="popover"],[data-original-title]').each(function () {
           //the 'is' for buttons that trigger popups
           //the 'has' for icons within a button that triggers a popup
           if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
              (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false;  // fix for BS 3.3.6
           }
      });
  });
  
  //Delete files
  $(document).on('click', '.popover-content-remove', function (e) {
      //Get the id whose file to delete
      let id = $(this).attr('data-target');

      //Get the name of the file to delete
      let name = $(this).attr('data-name');

      //Confirm delete
      let isDelete = confirm("Você realmente quer apagar este arquivo?");  

      //If confirmed
      if (isDelete) {
         //Remove the requested file
         let files = Object.values(fileStorage[id]);
         let newArr = files.filter((e) => { return e.name !== name; });

         //Update the list
         storeFile(id, newArr);

          //If there is no file then show No file
          if(newArr.length === 0){
                $(this).parent().parent().append("<div class='col-12 pb10'><span class='popover-content'>Nenhum Vídeo</span></div>");
           }

           //Remove the current file
           $(this).parent().remove();
        }
  });
});

</script>

<script defer src="public/js/fontawersome/all.js" integrity="sha384-0pzryjIRos8mFBWMzSSZApWtPl/5++eIfzYmTgBBmXYdhvxPc+XcFEk+zJwDgWbP" crossorigin="anonymous"></script>