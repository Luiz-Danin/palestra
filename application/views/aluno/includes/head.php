<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <!-- Theme Made By www.w3schools.com - No Copyright -->
        <title>Palestras</title>
        <base href="<?= base_url() ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<link rel="stylesheet" href="public/css/bootstrap.min.css">-->
        <link rel="stylesheet" href="public/css/bootstrap/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="public/css/fontawersome/css/font-awesome.min.css">

        <!--<script src="public/js/jquery/jquery-3.4.0.min.js"></script>-->
        <script src="public/js/jquery/jquery-3.4.1.min.js"></script>
        <script src="public/js/popper.min.js"></script>
        <script src="public/js/bootstrap/js/bootstrap.min.js"></script>
        <script src="public/js/typeahead.js"></script>
<!--        <script defer src="public/js/fontawersome/all.js" integrity="sha384-0pzryjIRos8mFBWMzSSZApWtPl/5++eIfzYmTgBBmXYdhvxPc+XcFEk+zJwDgWbP" crossorigin="anonymous"></script>-->
        <style>
/*            .typeahead { border: 2px solid #FFF;border-radius: 4px;padding: 8px 12px;max-width: 300px;min-width: 290px;background: rgba(66, 52, 52, 0.5);color: #FFF;}
            .tt-menu { width:300px; }
            ul.typeahead{margin:0px;padding:10px 0px;}
            ul.typeahead.dropdown-menu li a {padding: 10px !important;	border-bottom:#CCC 1px solid;color:#FFF;}
            ul.typeahead.dropdown-menu li:last-child a { border-bottom:0px !important; }
            .bgcolor {max-width: 550px;min-width: 290px;max-height:340px;background:url("world-contries.jpg") no-repeat center center;padding: 100px 10px 130px;border-radius:4px;text-align:center;margin:10px;}
            .demo-label {font-size:1.5em;color: #686868;font-weight: 500;color:#FFF;}
            .dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover {
                    text-decoration: none;
                    background-color: #1f3f41;
                    outline: 0;
            }*/
        </style>
        <style>
            html{
                overflow-x: hidden;
            }
            .navbar-dark .navbar-nav .nav-link
            {
                color:#FFF;
            }
        </style>
    </head>
    <header>
<!--        <nav class="navbar navbar-expand-lg navbar-light bg-light">-->
        <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="<?php echo base_url(); ?>inicio"><img style="width: 65px;" class="logo_palestra" src="<?php echo base_url();?>public/img/logo_palestra.png"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>inicio"><i class="fa fa-home"></i>
                            Minhas Palestras<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>pagina_cadastro_palestra"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Cadastrar Palestra</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>palestra_disponivel"><i class="fa fa-play-circle"></i>
                            Palestras Disponiveis</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>palestra_matriculada"><i class="fa fa-graduation-cap"></i>Palestras Matriculadas</a>
                    </li>
                    <!--      <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Dropdown
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                          </li>-->
                    <!--      <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                          </li>-->
                </ul>
                <form method="post" action="<?php echo base_url();?>busca_curso" class="form-inline my-2 my-lg-0">
                <div class="input-group">
                    <input id="id_busca" name="busca" required="" autocomplete="off" type="text" class="form-control" placeholder="Buscar Palestra" aria-label="Buscar Palestras" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                </div>
                </form>
<!--                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Buscar Palestras" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>-->
            </div>
        </nav>
    </header>
<script>

var base_url = "<?php echo base_url(); ?>";

    $(document).ready(function () {
        $('#id_busca').typeahead({
            source: function (search, result) {
                
                $.ajax({
                    url: base_url + "lista_cursos",
                    data: 'search=' + search,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        
                    result($.map(data, function (item) {
							return item;
                        }));
                    },
                    error: function()
                    {
                        console.log("error!");
                    }
                });
            }
        });
    });
</script>