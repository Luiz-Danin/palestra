<?php $this->load->view('aluno/includes/head.php'); ?>
<!--<link rel="stylesheet"  href="public/css/animate.css">-->

<style>
    /* Tabs*/
section {
    padding: 60px 0;
}

section .section-title {
    text-align: center;
    color: #007b5e;
    margin-bottom: 50px;
    text-transform: uppercase;
}
#tabs{
	background: #fff;
    color: #eee;
}
#tabs h6.section-title{
    color: #212529;
}

#tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #f3f3f3;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 4px solid !important;
    font-size: 20px;
    font-weight: bold;
}
#tabs .nav-tabs .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #212529;
    font-size: 20px;
}

label
{
    color:#000;
}

.tab-content>.active 
{
    color:#000;
}

/*botão assistir video*/
.page {
    padding: 15px 0 0;
}

.bmd-modalButton {
/*    display: block;
    margin: 15px auto;
    padding: 5px 15px;*/
}

.close-button {
    overflow: hidden;
}

.bmd-modalContent {
    box-shadow: none;
    background-color: transparent;
    border: 0;
    /*margin-top: 200px;*/
}

.bmd-modalContent .close {
    font-size: 30px;
    line-height: 30px;
    padding: 7px 4px 7px 13px;
    text-shadow: none;
    opacity: .7;
    color:#f00;
}

.bmd-modalContent .close span {
    display: block;
}

.bmd-modalContent .close:hover,
.bmd-modalContent .close:focus {
    opacity: 1;
    outline: none;
}

.bmd-modalContent iframe {
    display: block;
    margin: 0 auto;
}

/*btn add*/
.btn-glyphicon { padding:8px; background:#ffffff; margin-right:4px; }
.icon-btn { padding: 1px 15px 3px 2px; border-radius:50px;}
</style>
<!-- Tabs -->
<section id="tabs">
	<div class="container-fluid">
            
            <?php
            if (isset($_SESSION['feedback_request'])) {
                ?>

            <div class="container">
                <div class="alert alert-success">
                  <?= $_SESSION['feedback_request'] ?>.
                </div>
            </div>

            <?php
                    unset($_SESSION['feedback_request']);
                }
            ?>
            <h2><p class="text-center" style="color:black;">Obtenha o Certificado de Palestrante</p></h2>
		<!--<h6 class="section-title h1">Palestras</h6>-->
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Minhas Palestras Cadastradas</a>
						<!--<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Cadastrar Palestras</a>-->
						<!--<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Palestras Disponíveis</a>-->
						<!--<a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">About</a>-->
					</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                        <p style="margin-left:15px; margin-right: 15px">Bem-vindo ao Palestras! <a class="btn btn-outline-success" href="<?php echo base_url().'pagina_cadastro_palestra'; ?>">Cadastrar sua Palestra <i class="fa fa-video-camera" aria-hidden="true"></i></a></p>
                                        
                                        <!--inicio saiba mais-->
                                        <!--<button class="btn btn-outline-primary" data-toggle="modal" data-target=".animate" data-ui-class="a-fadeRight"><i class="fa fa-info-circle" aria-hidden="true"></i> Saiba mais...</button>-->
                                        <!--fim saiba mais-->
                                        
                                        <div class="container-fluid">
                                            <?php
                                            if(count($minhas_palestas)>0)
                                            {
                                            ?>
<!--                                            <h2>Basic Table</h2>
                                            <p>The .table class adds basic styling (light padding and horizontal dividers) to a table:</p>            -->
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Palestra</th>
                                                        <th>Situação</th>
                                                        <th>Certificado</th>
                                                        <th>Vídeo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    
                                                        foreach ($minhas_palestas as $palestras)
                                                        {  
                                                            if( isset($palestras['link_palestra']) )
                                                            {
                                                                
                                                            echo '<tr>';
                                                            echo '<td>'.$palestras['nome'].'</td>';
                                                            switch ($palestras['situacao']):
                                                                case 0:
                                                                    $situacao = '<td>Indeferida</td>';
                                                                    $certificado = '<td> </td>';
                                                                    $link = '<td></td>';
                                                                    //$link = 'https://www.youtube.com/embed/6aFdEhEZQjE';
                                                                    break;
                                                                case 1:
                                                                    $situacao = '<td>Em analise</td>';
                                                                    $certificado = '<td> </td>';
                                                                    $link = '<td></td>';
                                                                    //$link = 'https://www.youtube.com/embed/vI4LHl4yFuo';
                                                                    break;
                                                                case 2:
                                                                    $situacao = '<td>Aprovada</td>';
                                                                    
                                                                    if($palestras['avaliacao']>=5)
                                                                    {
                                                                        
                                                                        if( isset($palestras['pago']) && ($palestras['pago']==TRUE))
                                                                        {
                                                                            $certificado = '<td>'
                                                                                    . '<a target="_blank" href="'.$palestras['url_certificado'].'" class="btn btn-outline-success enviar_ovumdocs" role="button">Certificado</a>'
                                                                                            //. '<button value="'.codifica( $palestras['id_historico'] ).'" id="'.$palestras['id_palestra'].'" type="submit" class="btn btn-outline-success enviar_ovumdocs" data-toggle="modal" data-target="#exampleModal">Certificado</button>'
                                                                                    . '</td>';
                                                                        }
                                                                        else if(isset($palestras['pago']) && ($palestras['pago']==FALSE))
                                                                        {
                                                                            $certificado = '<td> <form method="post" class="form-inline" action="'. base_url().'pagamento_palestrante">'
                                                                                            . '<input value="'.codifica( $palestras['id_palestra'] ).'" type="hidden" name="id_palestra">'
                                                                                            . '<button type="submit" class="btn btn-outline-primary">Certificado</button>'
                                                                                            . '</form>'
                                                                                            . '</td>';
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if($palestras['avaliacao']==0)
                                                                        {   
                                                                            $avaliacao = 'Nenhuma Avaliação Ainda';
                                                                        }
                                                                        elseif ($palestras['avaliacao']==1)
                                                                        {
                                                                            $avaliacao = 5- $palestras['avaliacao'];
                                                                            $avaliacao = 'Faltam '.$avaliacao.' Avaliações';
                                                                        }
                                                                        else
                                                                        {
                                                                            $avaliacao = 5- $palestras['avaliacao'];
                                                                           $avaliacao = 'Faltam '.$avaliacao.' Avaliações';
                                                                        }
                                                                        
                                                                        $certificado = '<td><button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="É necessário receber 5 avaliações para liberação do Certificado"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> '.$avaliacao.' </button></td>';
                                                                    }
                                                                    
                                                                    $link = '<td><button type="button" class="btn btn-outline-dark bmd-modalButton" data-toggle="modal" data-bmdSrc="'.$palestras['link_palestra'].'/'.base64_encode($identificador_usuario).'" data-bmdWidth="640" data-bmdHeight="480" data-target="#myModal"  data-bmdVideoFullscreen="true">Assistir</button></td>';
                                                                    //$link = 'https://www.youtube.com/embed/cMNPPgB0_mU';
                                                                    break;
                                                            endswitch;
                                                            
                                                            echo $situacao;
                                                            echo $certificado;
                                                            echo $link;
                                                            echo '</tr>';
                                                            }
                                                        }
                                                            echo '</tbody>';
                                                        echo '</table>';
                                                        echo '</div>';
                                                    }
                                                    else
                                                    {
                                                        echo '<div class="alert alert-secondary">
                                                                Você não possui nenhuma palestra cadastrada.<strong><a href="'. base_url().'pagina_cadastro_palestra">Cadastre uma palestra!</a></strong>
                                                              </div>';
                                                    }
                                                    ?>
                                        </div>



                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
<!--                                        <form method="post" enctype="multipart/form-data" action="<?php //echo base_url();?>send">
                                            <br>
                                            <br>
                                            <label for="title">Titulo do Vídeo:</label>
                                            <input class="form form-control"  type="text" name="video[titulo]" maxlength="100" value="" />
                                            <br>
                                            <label for="description">Descrição para o Vídeo:</label>
                                            <textarea class="form form-control"  name="video[descricao]" cols="20" rows="1" maxlength="5000" ></textarea>
                                            <br>
                                            <label for="tags">Tags: [ separa por ';' ]</label>
                                            <input class="form form-control"  type="text" name="video[tags]" value="" />
                                            <br>
                                            <label for="tags">Classificação indicativa: (Op)</label><input class="form form-control" type="text" name="video[classificacao_indicativa]" value="">
                                            <br>
                                            <label for="tags">URL de Retorno: (Op)</label>
                                            <input class="form form-control" type="text" name="video[url_retorno]" value="">
                                            <br>
                                            <label for="file">Arquivo de Video:</label> 
                                            <input  class="form form-control" type="file" name="arquivo" >
                                            <br>
                                            <button class="btn btn-primary btn-block" type="submit">Enviar</button>
                                        </form>-->
                                    </div>
					<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
						
                                            
                                        </div>
					</div>
<!--					<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
						Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
					</div>-->
				</div>
			
			</div>
		</div>
        <?php 
             if (isset($minhas_palestas['links']))
             {
                echo $minhas_palestas['links']; 
             }
        ?>
	</div>
</section>
<!-- ./Tabs -->
<footer>
	<div class="modal fade" id="myModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content bmd-modalContent">

				<div class="modal-body">
          
          <div class="close-button">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="embed-responsive embed-responsive-16by9">
					            <iframe allowfullscreen width = "560px !important" height = "315px !important" class="embed-responsive-item" frameborder="0"></iframe>
          </div>
				</div>

			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
  
      </footer>

<div id="exampleModalLabel" class="modal animate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="true">
    <div class="modal-lg modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Como enviar sua Palestra?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body text-center p-lg">
                <p>Are you sure to execute this action?</p>
                <i class="fa fa-circle" aria-hidden="true"></i>
                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                <i class="fa fa-circle-thin" aria-hidden="true"></i>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>
                <button id="id_proximo_1" type="button" class="btn btn-primary" data-toggle="modal" data-target=".animate2" data-ui-class="a-fadeRightBig">Proximo <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>

<div id="exampleModalLabel2" class="modal animate2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true" data-backdrop="true">
    <div class="modal-lg modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Como enviar sua Palestra?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body text-center p-lg">
                <p>Are you sure to execute this action?</p>
                <i class="fa fa-circle" aria-hidden="true"></i>
                <i class="fa fa-circle" aria-hidden="true"></i>
                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                <i class="fa fa-circle-thin" aria-hidden="true"></i>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Proximo <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>

<script>

$("#id_proximo_1").click( function(){
    
    $('#exampleModalLabel').modal('hide');
    
});
    
    
$(function(){
    
	$('[role=dialog]')
	    .on('show.bs.modal', function(e) {
		    $(this)
		        .find('[role=document]')
		            .removeClass()
		            .addClass('modal-dialog ' + $(e.relatedTarget).data('ui-class'))
	})
})    
</script>

<script>
$(document).ready(function(){
    
$("button.matricula-palestra").on('click', function(e){

    var base_url = "<?php echo base_url(); ?>";
    var valor = $(this).attr('value');
          
    $.ajax({
        method: "POST",
        url: base_url+"matricula",
        dataType: "html",
        data: { valor:valor },
        beforeSend: function() {
            //$('.enviar-curso').prop('disabled', true);
            //$('.btn-matricula').html('<button class="btn btn-success" disabled><span class="spinner-border spinner-border-sm"></span>Aguarde..</button>');
        },
        success:function (response) {
                //console.log(response);
                 //event.preventDefault();

            $(".exibe-palestra-matricula").hide().html(response).fadeIn("slow");
            //$(".exibe-palestra-matricula").load();
            //document.location.href = document.location.href;
            
            //document.location.href = $("a[data-toggle=tab][href=#nav-contact-tab]").tab("show");
            
        },
        complete: function (data) {
            //completeAjax(); 
        }
    });
          
    });
    
    });
</script>
<script>
$(function () {
  $('[data-toggle="popover"]').popover()
})
    
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<script>

(function($) {
    
    $.fn.bmdIframe = function( options ) {
        var self = this;
        var settings = $.extend({
            classBtn: '.bmd-modalButton',
            defaultW: 640,
            defaultH: 360
        }, options );
      
        $(settings.classBtn).on('click', function(e) {
          var allowFullscreen = $(this).attr('data-bmdVideoFullscreen') || false;
          
          var dataVideo = {
            'src': $(this).attr('data-bmdSrc'),
            'height': $(this).attr('data-bmdHeight') || settings.defaultH,
            'width': $(this).attr('data-bmdWidth') || settings.defaultW
          };
          
          if ( allowFullscreen ) dataVideo.allowfullscreen = "";
          
          // stampiamo i nostri dati nell'iframe
          $(self).find("iframe").attr(dataVideo);
        });
      
        // se si chiude la modale resettiamo i dati dell'iframe per impedire ad un video di continuare a riprodursi anche quando la modale è chiusa
        this.on('hidden.bs.modal', function(){
          $(this).find('iframe').html("").attr("src", "");
        });
      
        return this;
    };
  
})(jQuery);




jQuery(document).ready(function(){
  jQuery("#myModal").bmdIframe();
});
</script>