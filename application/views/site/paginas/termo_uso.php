<head>
    <!-- Theme Made By www.w3schools.com - No Copyright -->
    <title>Palestras</title>
    <base href="<?= base_url() ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="public/css/bootstrap.min.css">

    <script src="public/js/jquery/jquery-3.4.0.min.js"></script>
    <script src="public/js/bootstrap.min.js"></script>
</head>
<style>
    
    body
    {
        background-color: #fff;
    }
    
    div.controls {
    padding: 10px 30px;
    margin-bottom: 10px;
}

div.container {
    height: 600px;
    width: 600px;
    /*overflow: scroll;*/
    /*border: 1px solid blue;*/
}

div.container-fluid {
    height: 500px;
    width: 600px;
        overflow: scroll;
    border: 1px solid #333;
    background-color: white;
}

div.container p {
    font-size: 20px;
    line-height: 2em;
    margin-top: 0px;
}

/*preloader*/
.loadText{
    font-family: Arial;
    color:#000;
	font-size: 30px;
}
.preloader{
    background-color:#FFF;
    height: 100%;
    width: 100%;
	padding-top: 60px;
	/*margin-left: -20px;*/
	/*margin-top: -20px;*/
	position: fixed;
/*	width: 100%;*/
	z-index: 2;
}
</style>
<div class="preloader" id="preloader">
    <img class="img-responsive center-block">
	<p class="text-center loadText">Aguarde...</p>
</div>
<div class="palestra-corpo">
    <div class="container">
        <div class="row">
            <div class="controls">
                <h2 class="text-center">Aceitar Termo de Uso?</h2>
                <form class="form-inline" method="post" action="<?php echo base_url(); ?>aceite">
                    <button type="submit" value="1" class="btn btn-success">SIM</button>
                    <button type="button" class="btn btn-danger">NÃO</button>
                    <input type="hidden" name="termo_uso" value="<?php echo codifica($termo_uso_ativo['id_termo_uso']); ?>">
                    <input type="hidden" name="aceite" value="<?php echo codifica('1'); ?>">
                    
                    <input type="hidden" name="request" value="<?php echo codifica($request);?>">
                    <input type="hidden" name="id_instituicao" value="<?php echo codifica($id_instituicao);?>">
                </form>
            </div>
            <div class="container-fluid">
                <p> <?php echo $termo_uso_ativo['texto']; ?></p>
            </div>
        </div>
    </div>
</div>
<script>

$(document).ready(function(){
    setTimeout(function() {
		$("#preloader").fadeOut();
	},3000);
});</script>
<script>
function scroll_down() {
    var el = document.querySelector('.container');
    smooth_scroll_to(el, el.scrollTop + 200, 600);
}

function scroll_up() {
    var el = document.querySelector('.container');
    smooth_scroll_to(el, el.scrollTop - 200, 600);
}


/**
    Smoothly scroll element to the given target (element.scrollTop)
    for the given duration

    Returns a promise that's fulfilled when done, or rejected if
    interrupted
 */
var smooth_scroll_to = function(element, target, duration) {
    target = Math.round(target);
    duration = Math.round(duration);
    if (duration < 0) {
        return Promise.reject("bad duration");
    }
    if (duration === 0) {
        element.scrollTop = target;
        return Promise.resolve();
    }

    var start_time = Date.now();
    var end_time = start_time + duration;

    var start_top = element.scrollTop;
    var distance = target - start_top;

    // based on http://en.wikipedia.org/wiki/Smoothstep
    var smooth_step = function(start, end, point) {
        if(point <= start) { return 0; }
        if(point >= end) { return 1; }
        var x = (point - start) / (end - start); // interpolation
        return x*x*(3 - 2*x);
    }

    return new Promise(function(resolve, reject) {
        // This is to keep track of where the element's scrollTop is
        // supposed to be, based on what we're doing
        var previous_top = element.scrollTop;

        // This is like a think function from a game loop
        var scroll_frame = function() {
            if(element.scrollTop != previous_top) {
                reject("interrupted");
                return;
            }

            // set the scrollTop for this frame
            var now = Date.now();
            var point = smooth_step(start_time, end_time, now);
            var frameTop = Math.round(start_top + (distance * point));
            element.scrollTop = frameTop;

            // check if we're done!
            if(now >= end_time) {
                resolve();
                return;
            }

            // If we were supposed to scroll but didn't, then we
            // probably hit the limit, so consider it done; not
            // interrupted.
            if(element.scrollTop === previous_top
                && element.scrollTop !== frameTop) {
                resolve();
                return;
            }
            previous_top = element.scrollTop;

            // schedule next frame for execution
            setTimeout(scroll_frame, 0);
        }

        // boostrap the animation process
        setTimeout(scroll_frame, 0);
    });
}
</script>