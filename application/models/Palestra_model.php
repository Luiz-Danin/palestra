<?php
defined('BASEPATH') OR exito('No direct script access allowed');

class Palestra_model extends CI_Model
{
    public function termo_uso($dados)
    {
        $termo = ['texto'=>$dados];
        $this->db->insert("termo_uso", $termo);
    }
    
    public function get_usuario($id_usuario)
    {
         $this->db->select();
        $this->db->from('usuario');
        $this->db->where('usuario.id_usuario', $id_usuario);
        
        $usuario = $this->db->get()->result_array();
        return $usuario[0];
    }
    
    public function is_pago($id_usuario, $id_historico)
    {
        $this->db->select();
        $this->db->from('historico');
        
        $this->db->join('usuario_pagamento', 'historico.id_historico = usuario_pagamento.id_historico');
        
        $this->db->where('usuario_pagamento.id_usuario', $id_usuario);
        $this->db->where('usuario_pagamento.id_historico', $id_historico);
        $this->db->where('usuario_pagamento.situacao', '3');
        
        $pago  = $this->db->get()->result_array();
        
        if( count($pago[0])>0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function get_certificado_palestra($id_certificado, $id_usuario, $id_palestra, $id_historico)
    {
        $this->db->select("usuario.nome as nome, usuario.documento , palestra.nome as nome_palestra, palestra.descricao as descricao_palestra, "
                . "palestra.data_criacao as data_palestra, palestra.descricao descricao_palestra,palestra.id_usuario_palestrante as id_usuario_palestrante,"
                . "certificado.id_certificado as id_certificado, certificado.codigo_autenticacao,certificado.id_historico as id_historico, certificado.identificador as identificador,"
                . "palestra_avaliacao.qualificacao as qualificacao, palestra_avaliacao.data as data_avaliacao");
        $this->db->from('historico');
        
        $this->db->join('palestra', 'historico.id_palestra = palestra.id_palestra');
        $this->db->join('usuario', 'historico.id_usuario = usuario.id_usuario');
        $this->db->join('certificado', 'historico.id_historico = certificado.id_historico');
        $this->db->join('palestra_avaliacao', 'historico.id_historico = palestra_avaliacao.id_historico');
        
        $this->db->where('palestra.id_palestra', $id_palestra);
        $this->db->where('usuario.id_usuario', $id_usuario);
        $this->db->where('historico.id_historico', $id_historico);
        //$this->db->where('palestra_avaliacao.id_historico', $id_historico);
        $this->db->where('certificado.id_certificado', $id_certificado);
        
        return $this->db->get()->result_array();
    }
    
    public function get_certificado_palestra_palestrante($id_certificado, $id_usuario, $id_palestra, $id_historico)
    {
        $this->db->select("usuario.nome as nome, usuario.documento , palestra.nome as nome_palestra, palestra.descricao as descricao_palestra, "
                . "palestra.data_criacao as data_palestra, palestra.descricao descricao_palestra, palestra.id_usuario_palestrante as id_usuario_palestrante,"
                . "certificado.id_certificado as id_certificado, certificado.codigo_autenticacao,certificado.id_historico as id_historico, certificado.identificador as identificador");
        $this->db->from('historico');
        
        $this->db->join('palestra', 'historico.id_palestra = palestra.id_palestra');
        $this->db->join('usuario', 'historico.id_usuario = usuario.id_usuario');
        $this->db->join('certificado', 'historico.id_historico = certificado.id_historico');
        //$this->db->join('palestra_avaliacao', 'historico.id_historico = palestra_avaliacao.id_historico');
        
        $this->db->where('palestra.id_palestra', $id_palestra);
        $this->db->where('usuario.id_usuario', $id_usuario);
        $this->db->where('historico.id_historico', $id_historico);
        //$this->db->where('palestra_avaliacao.id_historico', $id_historico);
        $this->db->where('certificado.id_certificado', $id_certificado);
        
        return $this->db->get()->result_array();
    }

    public function edit_situacao_palestra($id_palestra, $situacao)
    {
        $this->db->set('situacao', $situacao);
        $this->db->where('id_palestra', $id_palestra);
        
        $this->db->update('palestra');
    }
    
    public function consulta_palestra_identificador($identificador)
    {
        
        $this->db->select("palestra.id_palestra");
        $this->db->from('modulo_palestra');
        
        $this->db->join('palestra', 'modulo_palestra.id_palestra = palestra.id_palestra');
        $this->db->join('modulo', 'modulo_palestra.id_modulo = modulo.id_modulo');
        $this->db->join('material_modulo', 'modulo.id_modulo = material_modulo.id_modulo');
        $this->db->join('material', 'material_modulo.id_material = material.id_material');
        
        $this->db->where('material.identificador', $identificador);
        
        return $this->db->get()->result_array();
    }

    public function add_material_modulo($id_material, $id_modulo)
    {
        $material_modulo = ['id_material'=>$id_material, 'id_modulo'=>$id_modulo ];
        return $this->db->insert("material_modulo", $material_modulo);
    }

    public function add_material($idenficador, $url)
    {
        $material = ['identificador'=>$idenficador, 'url'=>$url,'situacao'=>1 ];
        $this->db->insert("material", $material);
        
        return $this->db->insert_id();
    }

    public function add_modulo_palestra($id_modulo, $id_palestra)
    {
        $modulo_palestra = ['id_modulo'=>$id_modulo, 'id_palestra'=>$id_palestra ];
        $this->db->insert("modulo_palestra", $modulo_palestra);
    }

    public function add_modulo()
    {
        $modulo = ['nome'=>'Modulo 1', 'situacao'=>1 ];
        $this->db->insert("modulo", $modulo);
        
        return $this->db->insert_id();
    }
    
    private  function limitarTexto($texto, $limite)
    {
       $contador = strlen($texto);
       
       if ($contador >= $limite)
       {
           $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' '));
           return $texto;
       }
       else
       {
           return $texto;
       }
   }
   
   public function add_palestra($id_usuario_palestrante, $nome, $descricao, $location)
   {
        $descricao = $this->limitarTexto($descricao, 250);
        
        $palestra = ['id_usuario_palestrante'=>$id_usuario_palestrante, 'nome'=>$nome, 'data_criacao'=>date("Y-m-d H:i:s"), 'privada'=>0, 'dt_inicio'=>date("Y-m-d H:i:s"), 'local'=>$location,'descricao'=>$descricao, 'situacao'=>1 ];
        $this->db->insert("palestra", $palestra);
        
        return $this->db->insert_id();
    }

    public function add_palestra_avaliacao_cupom($id_avaliacao, $cupom)
    {
        $avaliacao_palestra = ['id_avaliacao'=>$id_avaliacao, 'cupom'=>$cupom, 'situacao'=>1 ];
        $this->db->insert("palestra_avaliacao_cupom", $avaliacao_palestra);
        
        //return $this->db->insert_id();
    }

    public function avaliacao_palestra($id_aluno, $valor, $id_historico, $id_palestra)
    {
        $consulta_avaliacao_palestra = $this->consulta_avaliacao_palestra($id_aluno, $id_historico, $id_palestra);
        
        if( count($consulta_avaliacao_palestra)>0 )
        {
            //var_dump($consulta_avaliacao_palestra[0][0]);die;
            //echo json_encode($consulta_avaliacao_palestra[0]);
            return ['status'=>TRUE, 'id_avaliacao'=>$consulta_avaliacao_palestra[0]];
        }
        else
        {
            //return FALSE;
            $id_avaliacao_palestra = $this->add_avaliacao_palestra($id_aluno, $valor, $id_historico, $id_palestra);
            return ['status'=>FALSE ,'id_avaliacao'=>$id_avaliacao_palestra];
        }
    }

    public function add_avaliacao_palestra($id_aluno, $valor, $id_historico, $id_palestra)
    {
        $avaliacao_palestra = ['id_palestra'=>$id_palestra, 'id_historico'=>$id_historico, 'id_aluno'=>$id_aluno, 'qualificacao'=>$valor, 'data'=>date("Y-m-d H:i:s") ];
        $this->db->insert("palestra_avaliacao", $avaliacao_palestra);
        
        return $this->db->insert_id();
    }
    
    public function consulta_avaliacao_palestra($id_aluno, $id_historico, $id_palestra)
    {
        $this->db->select();
        $this->db->from('palestra_avaliacao');
        $this->db->where('id_aluno', $id_aluno);
        $this->db->where('id_historico', $id_historico);
        $this->db->where('id_palestra', $id_palestra);
        
        return $this->db->get()->result_array();
    }
    
    public function consulta_palestra_avaliacao_cupom($id_avaliacao)
    {
        $this->db->select();
        $this->db->from('palestra_avaliacao_cupom');
        $this->db->where('id_avaliacao', $id_avaliacao);
        
        return $this->db->get()->result_array();
    }

    public function add_historico_participante($id_usuario, $id_palestra)
    {
        $historico = ['id_usuario'=>$id_usuario, 'id_palestra'=>$id_palestra, 'data'=>date("Y-m-d H:i:s"), 'tipo'=>2, 'situacao'=>1];
        return $this->db->insert("historico", $historico);
    }

    public function update_situacao_pagamento($id_transacao, $codigo_intermediador, $situacao)
    {
        $this->db->set('situacao', $situacao);
        $this->db->where('id_transacao', $id_transacao);
        $this->db->where('codigo_intermediador', $codigo_intermediador);
        
        $this->db->update('usuario_pagamento');
    }
    
    public function check_avaliacao_palestra($id_palestra)
    {
        $this->db->select();
        $this->db->from('palestra_avaliacao');
        //$this->db->join('historico','palestra_avaliacao.id_palestra = historico.id_palestra');
        
        $this->db->where('palestra_avaliacao.id_palestra', $id_palestra);
        
        return $this->db->get()->result_array();
    }
    
    public function add_usuario_participante_pagamento($id_usuario, $id_historico, $id_transacao, $codigo_intermediador)
    {
        $usuario_pagamento = ['id_usuario'=>$id_usuario,'id_produto'=>2,'id_historico'=>$id_historico, 'id_transacao'=>$id_transacao, 'codigo_intermediador'=>$codigo_intermediador, 'data_criacao'=> date("Y-m-d H:i:s"),'situacao'=>1 ];
        
        $this->db->insert("usuario_pagamento", $usuario_pagamento);
    }

    public function add_usuario_palestrante_pagamento($id_usuario, $id_historico, $id_transacao, $codigo_intermediador)
    {
        $usuario_pagamento = ['id_usuario'=>$id_usuario,'id_produto'=>1,'id_historico'=>$id_historico, 'id_transacao'=>$id_transacao, 'codigo_intermediador'=>$codigo_intermediador, 'data_criacao'=> date("Y-m-d H:i:s"),'situacao'=>1 ];
        
        $this->db->insert("usuario_pagamento", $usuario_pagamento);
    }
    
    public function certificado($id_usuario, $id_historico, $cod_autenticacao, $identificador)
    {
        $certificado = $this->consulta_certificado($id_usuario, $id_historico);
        
        if( count($certificado)>0 )
        {
            return ['status'=> TRUE, 'codigo_autenticacao'=>$certificado[0]['codigo_autenticacao'] ];
        }
        else
        {
            $autenticacao = $this->add_certificado_palestrante($id_usuario, $id_historico, $cod_autenticacao, $identificador);
            
            return ['status'=> FALSE, 'codigo_autenticacao'=>$autenticacao ];
        }
    }
    
    private function consulta_certificado($id_usuario, $id_historico)
    {
        $this->db->select();
        $this->db->from('certificado');
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_historico', $id_historico);
        
        return $this->db->get()->result_array();
    }
    
    public function certificado_participante($id_usuario, $id_historico, $cod_autenticacao, $identificador)
    {
        $certificado = $this->consulta_certificado($id_usuario, $id_historico);
        
        if( count($certificado)>0 )
        {
            return ['status'=> TRUE, 'codigo_autenticacao'=>$certificado[0]['codigo_autenticacao'] ];
        }
        else
        {
            $autenticacao = $this->add_certificado_participante($id_usuario, $id_historico, $cod_autenticacao, $identificador);
            return ['status'=> FALSE, 'codigo_autenticacao'=>$autenticacao ];
        }
    }


    public function add_certificado_palestrante($id_usuario, $id_historico, $autenticacao, $identificador )
    {
        $certificado = ['id_usuario'=>$id_usuario,'id_produto'=>1,'id_historico'=>$id_historico, 'identificador'=>$identificador, 'codigo_autenticacao'=>$autenticacao, 'situacao'=>0 ];
        
        $this->db->insert("certificado", $certificado);
        return [ 'id_certificado'=>$this->db->insert_id(), 'codigo_autenticacao'=>$autenticacao ];
    }
    
    public function add_certificado_participante($id_usuario, $id_historico, $autenticacao, $identificador )
    {
        $certificado = ['id_usuario'=>$id_usuario,'id_produto'=>2,'id_historico'=>$id_historico, 'identificador'=>$identificador, 'codigo_autenticacao'=>$autenticacao, 'situacao'=>0 ];
        
        $this->db->insert("certificado", $certificado);
        return [ 'id_certificado'=>$this->db->insert_id(), 'codigo_autenticacao'=>$autenticacao ];
        //return $autenticacao;
    }

    public function add_historico($id_usuario, $id_palestra)
    {
        $historico = ['id_usuario'=>$id_usuario,'id_palestra'=>$id_palestra, 'data'=>date("Y-m-d H:i:s"),'tipo'=>1, 'situacao'=>0 ];
        $this->db->insert("historico", $historico);
        
        return $this->db->insert_id();
    }
    
    public function get_historico_usuario($id_usuario)
    {
        $this->db->select();
        $this->db->from('historico');
        $this->db->where('id_usuario', $id_usuario);
        
        $this->db->order_by("id_historico", "DESC");
        
        return $this->db->get()->result_array();
    }
    
    public function get_historico_usuario_2($id_usuario, $limit=null, $start=null) 
    {
        
        $this->db->select();
        $this->db->from('historico');
        $this->db->where('id_usuario', $id_usuario);
        
        $this->db->order_by("id_historico", "DESC");
        $this->db->limit($limit, $start);
        
        //$this->db->get('historico');
        //echo $this->db->last_query();die;
        
        return $this->db->get()->result_array();
        
        
    }
    
    public function get_palestra_by_id_palestra($id)
    {
        $this->db->select();
        $this->db->from('palestra');
        
        $this->db->where('id_palestra', $id);

        return $this->db->get()->result_array();
    }
    
    public function get_palestras_disponiveis()
    {
        $this->db->select();
        $this->db->from('palestra');
        $this->db->where('privada', 0);
        $this->db->where('situacao', 2);
        
        $this->db->order_by("id_palestra", 'DESC');
        
        return $this->db->get()->result_array();
    }
    
    public function get_palestras_disponiveis_2($limit = null, $start = null)
    {
        $this->db->select();
        $this->db->from('palestra');
        $this->db->where('privada', 0);
        $this->db->where('situacao', 2);
        
        $this->db->order_by("id_palestra", 'DESC');
        $this->db->limit($limit, $start);
        
        return $this->db->get()->result_array();
    }
    
    public function get_busca_palestra($busca)
    {
        //$this->db->query("SELECT * FROM  curso WHERE status=1 and (nome  like  '%".$params['nome']."%' or descricao like '% ".$params['nome']." %') ORDER BY LOCATE('".$nome."',nome)>0 desc")->result();
        //$this->db->order_by('locate(\'' . $aluno_area[$i] . '\',nome)>0 desc, position(\'' .$aluno_area[$i].'\'in nome) asc');
        
        $this->db->select();
        $this->db->from('palestra');
        $this->db->where('privada', 0);
        $this->db->where('situacao', 2);
        
        $this->db->group_start();
            $this->db->like('nome', $busca);
            $this->db->or_like('descricao', $busca);
        $this->db->group_end();
        
        $this->db->order_by('locate(\'' . $busca . '\',nome)>0 desc, position(\'' .$busca.'\'in nome) asc');
        $this->db->order_by('locate(\'' . $busca . '\',descricao)>0 desc, position(\'' . $busca . '\'in descricao) asc');
        //$this->db->order_by("id_palestra", 'DESC');
        //$this->db->get("palestra");
        //echo $this->db->last_query();die;
        
        return $this->db->get()->result_array();
    }
    
    public function get_busca_palestra_2($busca, $limit = null, $start = null)
    {
        $this->db->select();
        $this->db->from('palestra');
        $this->db->where('privada', 0);
        $this->db->where('situacao', 2);
        
        $this->db->group_start();
            $this->db->like('nome', $busca);
            $this->db->or_like('descricao', $busca);
        $this->db->group_end();
        
        $this->db->order_by('locate(\'' . $busca . '\',nome)>0 desc, position(\'' .$busca.'\'in nome) asc');
        $this->db->order_by('locate(\'' . $busca . '\',descricao)>0 desc, position(\'' . $busca . '\'in descricao) asc');
        $this->db->limit($limit, $start);
        
        return $this->db->get()->result_array();
    }

    public function check_pagamento_pastestra($id_usuario, $id_palestra)
    {
        /*SELECT * FROM `historico` h
        JOIN usuario u ON (h.id_usuario = u.id_usuario)
        JOIN palestra p ON (h.id_palestra = p.id_palestra)
        JOIN usuario_pagamento up ON (h.id_historico = up.id_historico)
        WHERE u.id_usuario = 44*/
        
        $this->db->select();
        $this->db->from('historico');
        
        $this->db->join("usuario", "historico.id_usuario = usuario.id_usuario");
        $this->db->join("palestra", "historico.id_palestra = palestra.id_palestra");
        $this->db->join("usuario_pagamento", "historico.id_historico = usuario_pagamento.id_historico");
        $this->db->join("produto", "usuario_pagamento.id_produto = produto.id_produto");
        
        //$this->db->where('historico.id_palestra', $id_historico);
        $this->db->where('usuario.id_usuario', $id_usuario);
        $this->db->where('palestra.id_palestra', $id_palestra);
        $this->db->where('usuario_pagamento.situacao', 3);
        $this->db->where('produto.id_produto', 1);
        
        return $this->db->get()->result_array();
        //echo '<pre>';var_dump($pagamento);die;
        /*if(count($pagamento)>0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }*/
    }
    
    public function check_pagamento_historico($id_usuario, $id_historico)
    {
        $this->db->select();
        $this->db->from('usuario_pagamento');
        
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_historico', $id_historico);
        $this->db->where('situacao', 3);
        
        $check_pagamento_historico = $this->db->get()->result_array();
        
        if( count($check_pagamento_historico)>0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function get_link_palestra($id_palestra)
    {
        /*
        SELECT * FROM modulo_palestra mp
        JOIN palestra p ON (mp.id_palestra = p.id_palestra)
        JOIN modulo m ON (mp.id_modulo = m.id_modulo)
        JOIN material_modulo mm ON (m.id_modulo = mm.id_modulo)
        JOIN material ma ON (mm.id_material = ma.id_material)
        WHERE p.id_palestra = 1
         */
        
        $this->db->select("material.url");
        $this->db->from('modulo_palestra');
        $this->db->join('palestra', 'modulo_palestra.id_palestra = palestra.id_palestra');
        $this->db->join('modulo', 'modulo_palestra.id_modulo = modulo.id_modulo');
        $this->db->join('material_modulo', 'modulo.id_modulo = material_modulo.id_modulo');
        $this->db->join('material', 'material_modulo.id_material = material.id_material');
        
        $this->db->where('palestra.id_palestra', $id_palestra);
        //$this->db->get();
        
        //echo $this->db->last_query();die;
        
        return $this->db->get()->result_array();
    }
    

    public function get_minha_palestra_2($usuario_id_unico, $limit=null, $start=null)
    {
        $this->db->select();
        $this->db->from('palestra');
        $this->db->where('id_usuario_palestrante', $usuario_id_unico);
        $this->db->order_by("id_palestra", "DESC");
        
        $this->db->limit($limit, $start);
        
        return $this->db->get()->result_array();
    }
    
    public function get_minha_palestra($usuario_id_unico)
    {
        $this->db->select();
        $this->db->from('palestra');
        $this->db->where('id_usuario_palestrante', $usuario_id_unico);
        $this->db->order_by("id_palestra", "DESC");
        
        return $this->db->get()->result_array();
    }
    
    public function get_usuario_palestrante($id_usuario)
    {
        $this->db->select();
        $this->db->from('usuario');
        $this->db->join('palestra', 'usuario.id_usuario = palestra.id_usuario_palestrante');
        $this->db->where('usuario.id_usuario', $id_usuario);
        $this->db->limit(1);
        
        $usuario_palestrante = $this->db->get()->result_array();
        return $usuario_palestrante[0];
    }

    public function get_usuario_id_unico($usuario_id_unico)
    {
        $this->db->select();
        $this->db->from('usuario');
        $this->db->where('id_unico', $usuario_id_unico);
        
        return $this->db->get()->result_array();
    }

    public function usuario($dados)
    {
        $dados_aluno = $this->consulta_usuario($dados);
        
        if( count($dados_aluno)==0 )
        {
            return $this->add_usuario($dados);
        }
        else
        {
            $session = array(
                        'usuario_id_unico' => $dados_aluno[0]['id_unico'],
                        'usuario_nome' => $dados_aluno[0]['nome'],
                        'usuario_email' => $dados_aluno[0]['email'],
                        'usuario_documento' => $dados_aluno[0]['documento'],
                        'usuario_logado' => TRUE
                    );
            $this->session->set_userdata($session);
            
            return $dados_aluno[0]['id_usuario'];
        }
    }

    public function consulta_usuario($dados_aluno)
    {
        $this->db->select();
        $this->db->from('usuario');
        $this->db->where('id_unico',$dados_aluno['usuario']['id_unico']);
        //$this->db->where('email',$dados_aluno['usuario']['email']);
        //$this->db->where('cpf',$dados_aluno['usuario']['cpf']);
        $this->db->where('situacao', 1);
        
        return $this->db->get()->result_array();
    }
    
    private function add_usuario($dados_aluno)
    {
        $usuario = ['id_unico'=>$dados_aluno['usuario']['id_unico'],'nome'=>$dados_aluno['usuario']['nome'], 'email'=>$dados_aluno['usuario']['email'], 'documento'=>$dados_aluno['usuario']['documento'], 'situacao_cadastro'=>1, 'situacao'=>1 ];
        $this->db->insert("usuario", $usuario);
        
        $session = array(
                        'usuario_id_unico' => $dados_aluno['usuario']['id_unico'],
                        'usuario_nome' => $dados_aluno['usuario']['nome'],
                        'usuario_email' => $dados_aluno['usuario']['email'],
                        'usuario_documento' => $dados_aluno['usuario']['documento'],
                        'usuario_logado' => TRUE
                    );
        $this->session->set_userdata($session);
        
        return $this->db->insert_id();
    }

    public function instituicao($dados_instituicao)
    {
        $instituicao = $this->consulta_instituicao($dados_instituicao);

        if( count($instituicao)==0 )
        {
            return $this->add_instituicao($dados_instituicao);
        }
        else
        {
            return $instituicao[0]['id_instituicao'];
        }
    }
    
    private function consulta_instituicao($dados_instituicao)
    {
        $this->db->select();
        $this->db->from('instituicao');
        $this->db->where('id_externo',$dados_instituicao['dados']['id']);
        $this->db->where('situacao',1);
        
        $instituicao = $this->db->get()->result_array();
        
        return $instituicao;
    }
    
    private function add_instituicao($dados_instituicao)
    {
        $instituicao = ['id_externo'=>$dados_instituicao['dados']['id'], 'nome'=>$dados_instituicao['dados']['nome'], 'identificador_externo'=>$dados_instituicao['dados']['identificador'], 'situacao'=>1 ];
        $this->db->insert("instituicao", $instituicao);
        
        return $this->db->insert_id();
    }
    
    public function instituicao_usuario($id_usuario, $id_instituicao)
    {
        $instituicao_usuario = $this->consulta_instituicao_usuario($id_usuario);
        
        if( count($instituicao_usuario)==0 )
        {
            $this->add_instituicao_usuario($id_usuario, $id_instituicao);
        }
    }

    private function consulta_instituicao_usuario($id_usuario)
    {
        $this->db->select();
        $this->db->from('instituicao_usuario');
        $this->db->where('id_usuario', $id_usuario);
        $instituicao_usuario = $this->db->get()->result_array();
        
        return $instituicao_usuario;
    }

    
    public function add_instituicao_usuario($id_usuario, $id_instituicao)
    {
        $instituicao_usuario = ['id_usuario'=>$id_usuario, 'id_instituicao'=>$id_instituicao];
        $this->db->insert("instituicao_usuario", $instituicao_usuario);
    }

    public function usuario_termo_uso($id_usuario)
    {
        $this->db->select();
        $this->db->from('usuario_termo_uso');
        $this->db->where('id_usuario', $id_usuario);
        $usuario_termo_uso = $this->db->get()->result_array();
        
        if( count($usuario_termo_uso)>0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }

    public function insert_usuario_termo_uso($dados)
    {
        return $this->db->insert("usuario_termo_uso", $dados);
    }

    public function termo_uso_ativo()
    {
        $this->db->select();
        $this->db->from('termo_uso');
        $this->db->where('situacao','1');
        $termo_uso_ativo = $this->db->get()->result_array();
        
        return $termo_uso_ativo[0];
    }
}