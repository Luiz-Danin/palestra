<?php

function codifica($string = null){
    for($i = 0; $i < 3; $i++){
        $string = strrev(base64_encode($string));
    }
    return $string;
}


function decodifica($string = null){
    for($i = 0; $i < 3; $i++){
        $string = strrev($string);
        $string = base64_decode($string);
    }
    return $string;
}